<?php

declare(strict_types=1);

namespace App\Task\Infrastructure\Task\Domain;

use App\Shared\Infrastructure\EventStore\EventStoreInterface;
use App\Task\Domain\Task\Event\TaskEventInterface;
use App\Task\Domain\Task\Task;
use App\Task\Domain\Task\TaskOutsideInterface;
use Grifix\Clock\ClockInterface;

final class TaskOutside implements TaskOutsideInterface
{
    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly ClockInterface $clock,
    ) {
    }

    public function publishEvent(TaskEventInterface $event): void
    {
        $this->eventStore->add($event, Task::class, $event->getTaskId());
    }

    public function getCurrentTime(): \DateTimeImmutable
    {
        return $this->clock->getCurrentTime();
    }
}
