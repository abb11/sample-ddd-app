<?php

declare(strict_types=1);

namespace App\Task\Infrastructure\Task\Domain\Repository\Doctrine;

use App\Shared\Infrastructure\Domain\Repository\Doctrine\AbstractRepository;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Domain\Task\Exception\TaskNotFoundException;
use App\Task\Domain\Task\Task;
use App\Task\Domain\Task\TaskOutsideInterface;
use App\Task\Domain\Task\TaskRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::postLoad, method: 'injectDependencies', entity: Task::class)]
final class TaskRepository extends AbstractRepository implements TaskRepositoryInterface
{
    protected static function entityDependencies(): array
    {
        return [
            TaskOutsideInterface::class,
        ];
    }

    public function get(Uuid $id): Task
    {
        $task = $this->em->find(Task::class, $id);

        if (null === $task) {
            throw new TaskNotFoundException($id);
        }

        return $task;
    }

    public function add(Task $task): void
    {
        $this->em->persist($task);
    }
}
