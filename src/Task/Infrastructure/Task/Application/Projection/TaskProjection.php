<?php

declare(strict_types=1);

namespace App\Task\Infrastructure\Task\Application\Projection;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Projection\Dto\TaskDto;
use App\Task\Application\Task\Projection\Exception\TaskNotFoundException;
use App\Task\Application\Task\Projection\TaskFilter;
use App\Task\Application\Task\Projection\TaskProjectionInterface;
use App\Task\Domain\Task\Task;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

final class TaskProjection implements TaskProjectionInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getTask(Uuid $id): TaskDto
    {
        $task = $this->createSelectQb()
            ->where('id = :id')
            ->setParameter('id', $id->toString())
            ->executeQuery()
            ->fetchAssociative();

        if (!$task) {
            throw new TaskNotFoundException($id->toString());
        }

        return $this->createTaskDto($task);
    }

    /**
     * {@inheritDoc}
     */
    public function findTasks(TaskFilter $filter): array
    {
        $tasks = $this->createSelectQb($filter)
            ->executeQuery()
            ->fetchAllAssociative();

        return array_map(fn ($task) => $this->createTaskDto($task), $tasks);
    }

    /**
     * {@inheritDoc}
     */
    public function countTasks(TaskFilter $filter): int
    {
        $qb = $this->createSelectQb($filter)
            ->select('count(*)')
            ->resetQueryPart('orderBy')
            ->setFirstResult(0)
            ->setMaxResults(null);

        return (int) $qb->executeQuery()->fetchOne();
    }

    private function createSelectQb(TaskFilter $filter = null): QueryBuilder
    {
        $taskTable = $this->entityManager->getClassMetadata(Task::class)->getTableName();

        $qb = $this->entityManager
            ->getConnection()
            ->createQueryBuilder()
            ->select('*')
            ->from($taskTable, 'task');

        if (null !== $filter) {
            if (null !== $filter->getTaskId()) {
                $qb->where('id = :id')
                    ->setParameter('id', $filter->getTaskId()->toString());
            }

            if (null !== $filter->getExecutionDay()) {
                $qb->andWhere('execution_day = :executionDay')
                    ->setParameter('executionDay', $filter->getExecutionDay()->toString());
            }

            $qb->setFirstResult($filter->getOffset());
            $qb->setMaxResults($filter->getPerPageLimit());

            $sortFieldMap = [
                TaskFilter::SORT_BY_EXECUTION_DAY => 'execution_day',
                TaskFilter::SORT_BY_CREATED_AT => 'created_at',
            ];

            $qb->orderBy($sortFieldMap[$filter->getSortBy()] ?? 'created_at', $filter->getSortDir() ?? 'asc');
        }

        return $qb;
    }

    private function createTaskDto(array $task): TaskDto
    {
        return new TaskDto(
            Uuid::fromString($task['id']),
            $task['title'],
            $task['priority'],
            $task['status'],
            new \DateTimeImmutable($task['created_at']),
            null !== $task['execution_day'] ? Date::fromString($task['execution_day']) : null,
        );
    }
}
