<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Projection\Exception;

class TaskNotFoundException extends \Exception
{
    private string $taskId;

    public function __construct(string $taskId)
    {
        parent::__construct(sprintf('Task with ID "%s" does not exist', $taskId));

        $this->taskId = $taskId;
    }

    public function getTaskId(): string
    {
        return $this->taskId;
    }
}
