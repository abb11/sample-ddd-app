<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Projection\Dto;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;

class TaskDto
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $title,
        public readonly string $priority,
        public readonly string $status,
        public readonly \DateTimeImmutable $createdAt,
        public readonly ?Date $executionDay,
    ) {
    }
}
