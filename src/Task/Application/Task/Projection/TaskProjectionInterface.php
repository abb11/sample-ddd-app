<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Projection;

use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Projection\Dto\TaskDto;
use App\Task\Application\Task\Projection\Exception\TaskNotFoundException;

interface TaskProjectionInterface
{
    /**
     * Get task by ID
     *
     * @throws TaskNotFoundException
     */
    public function getTask(Uuid $id): TaskDto;

    /**
     * Find tasks
     *
     * @return TaskDto[]
     */
    public function findTasks(TaskFilter $filter): array;

    /**
     * Count tasks
     */
    public function countTasks(TaskFilter $filter): int;
}
