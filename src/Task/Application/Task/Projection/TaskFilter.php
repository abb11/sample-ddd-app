<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Projection;

use App\Shared\Application\Filter\PaginationFilter;
use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;

/**
 * Tasks Filter
 */
class TaskFilter extends PaginationFilter
{
    public const SORT_BY_EXECUTION_DAY = 'executionDay';
    public const SORT_BY_CREATED_AT = 'createdAt';

    private ?Uuid $taskId = null;

    private ?Date $executionDay = null;

    public function getTaskId(): ?Uuid
    {
        return $this->taskId;
    }

    public function setTaskId(?Uuid $taskId): void
    {
        $this->taskId = $taskId;
    }

    public function getExecutionDay(): ?Date
    {
        return $this->executionDay;
    }

    public function setExecutionDay(?Date $executionDay): self
    {
        $this->executionDay = $executionDay;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public static function getAllowedSortCols(): array
    {
        return [
            self::SORT_BY_EXECUTION_DAY,
            self::SORT_BY_CREATED_AT,
        ];
    }
}
