<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Update;

use App\Task\Domain\Task\TaskRepositoryInterface;

final class UpdateTaskCommandHandler
{
    public function __construct(
        private readonly TaskRepositoryInterface $taskRepository,
    ) {
    }

    public function __invoke(UpdateTaskCommand $command): void
    {
        $task = $this->taskRepository->get($command->taskId);
        $task->update(
            $command->newTitle,
            $command->newPriority,
            $command->newExecutionDay,
        );
    }
}
