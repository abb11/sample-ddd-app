<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Update;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;

final class UpdateTaskCommand
{
    public function __construct(
        public readonly Uuid $taskId,
        public readonly string $newTitle,
        public readonly string $newPriority,
        public readonly ?Date $newExecutionDay = null,
    ) {
    }
}
