<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Cancel;

use App\System\ValueObject\Uuid\Uuid;

final class CancelTaskCommand
{
    public function __construct(
        public readonly Uuid $taskId,
    ) {
    }
}
