<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Cancel;

use App\Task\Domain\Task\Exception\TaskNotFoundException;
use App\Task\Domain\Task\TaskRepositoryInterface;

final class CancelTaskCommandHandler
{
    public function __construct(
        private readonly TaskRepositoryInterface $taskRepository,
    ) {
    }

    /**
     * @throws TaskNotFoundException
     */
    public function __invoke(CancelTaskCommand $command): void
    {
        $this->taskRepository->get($command->taskId)->cancel();
    }
}
