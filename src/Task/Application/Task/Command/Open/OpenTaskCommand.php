<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Open;

use App\System\ValueObject\Uuid\Uuid;

final class OpenTaskCommand
{
    public function __construct(
        public readonly Uuid $taskId,
    ) {
    }
}
