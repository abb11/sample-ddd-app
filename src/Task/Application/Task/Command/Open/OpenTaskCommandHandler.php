<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Open;

use App\Task\Domain\Task\Exception\TaskNotFoundException;
use App\Task\Domain\Task\TaskRepositoryInterface;

final class OpenTaskCommandHandler
{
    public function __construct(
        private readonly TaskRepositoryInterface $taskRepository,
    ) {
    }

    /**
     * @throws TaskNotFoundException
     */
    public function __invoke(OpenTaskCommand $command): void
    {
        $this->taskRepository->get($command->taskId)->open();
    }
}
