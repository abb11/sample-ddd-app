<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Close;

use App\System\ValueObject\Uuid\Uuid;

final class CloseTaskCommand
{
    public function __construct(
        public readonly Uuid $taskId,
    ) {
    }
}
