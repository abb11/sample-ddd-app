<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Close;

use App\Task\Domain\Task\Exception\TaskNotFoundException;
use App\Task\Domain\Task\TaskRepositoryInterface;

final class CloseTaskCommandHandler
{
    public function __construct(
        private readonly TaskRepositoryInterface $taskRepository,
    ) {
    }

    /**
     * @throws TaskNotFoundException
     */
    public function __invoke(CloseTaskCommand $command): void
    {
        $this->taskRepository->get($command->taskId)->close();
    }
}
