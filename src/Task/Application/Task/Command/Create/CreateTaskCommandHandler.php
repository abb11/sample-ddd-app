<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Create;

use App\Task\Domain\Task\TaskFactory;
use App\Task\Domain\Task\TaskRepositoryInterface;

final class CreateTaskCommandHandler
{
    public function __construct(
        private readonly TaskFactory $taskFactory,
        private readonly TaskRepositoryInterface $taskRepository,
    ) {
    }

    public function __invoke(CreateTaskCommand $command): void
    {
        $this->taskRepository->add($this->taskFactory->create(
            $command->taskId,
            $command->title,
            $command->priority,
            $command->executionDay,
        ));
    }
}
