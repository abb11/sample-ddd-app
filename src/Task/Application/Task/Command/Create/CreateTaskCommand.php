<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Command\Create;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;

final class CreateTaskCommand
{
    public function __construct(
        public readonly Uuid $taskId,
        public readonly string $title,
        public readonly string $priority,
        public readonly ?Date $executionDay = null,
    ) {
    }
}
