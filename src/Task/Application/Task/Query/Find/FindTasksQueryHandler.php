<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Query\Find;

use App\Task\Application\Task\Projection\TaskProjectionInterface;

final class FindTasksQueryHandler
{
    public function __construct(
        private readonly TaskProjectionInterface $taskProjection,
    ) {
    }

    public function __invoke(FindTasksQuery $query): FindTasksQueryResult
    {
        $tasks = [];
        $pageCount = 1;
        $totalTaskCount = $this->taskProjection->countTasks($query->filter);

        if ($totalTaskCount > 0) {
            $tasks = $this->taskProjection->findTasks($query->filter);
            $pageCount = (int) ceil($totalTaskCount / $query->filter->getPerPageLimit());
        }

        return new FindTasksQueryResult($pageCount, $totalTaskCount, $tasks);
    }
}
