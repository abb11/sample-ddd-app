<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Query\Find;

use App\Task\Application\Task\Projection\TaskFilter;

final class FindTasksQuery
{
    public function __construct(
        public readonly TaskFilter $filter,
    ) {
    }
}
