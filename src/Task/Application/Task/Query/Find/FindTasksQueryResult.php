<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Query\Find;

use App\Task\Application\Task\Projection\Dto\TaskDto;

final class FindTasksQueryResult
{
    /**
     * @param TaskDto[] $tasks
     */
    public function __construct(
        public readonly int $pageCount,
        public readonly int $totalTaskCount,
        public readonly array $tasks,
    ) {
    }
}
