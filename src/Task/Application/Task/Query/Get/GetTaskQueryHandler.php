<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Query\Get;

use App\Task\Application\Task\Projection\Exception\TaskNotFoundException;
use App\Task\Application\Task\Projection\TaskProjectionInterface;

final class GetTaskQueryHandler
{
    public function __construct(
        private readonly TaskProjectionInterface $taskProjection,
    ) {
    }

    /**
     * @throws TaskNotFoundException
     */
    public function __invoke(GetTaskQuery $query): GetTaskQueryResult
    {
        return new GetTaskQueryResult($this->taskProjection->getTask($query->taskId));
    }
}
