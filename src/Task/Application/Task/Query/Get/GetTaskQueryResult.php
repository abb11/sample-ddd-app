<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Query\Get;

use App\Task\Application\Task\Projection\Dto\TaskDto;

final class GetTaskQueryResult
{
    public function __construct(
        public readonly TaskDto $task,
    ) {
    }
}
