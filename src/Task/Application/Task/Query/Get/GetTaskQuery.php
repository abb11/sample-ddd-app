<?php

declare(strict_types=1);

namespace App\Task\Application\Task\Query\Get;

use App\System\ValueObject\Uuid\Uuid;

final class GetTaskQuery
{
    public function __construct(
        public readonly Uuid $taskId,
    ) {
    }
}
