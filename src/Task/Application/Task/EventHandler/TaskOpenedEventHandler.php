<?php

declare(strict_types=1);

namespace App\Task\Application\Task\EventHandler;

use App\Task\Domain\Task\Event\TaskOpenedEvent;

final class TaskOpenedEventHandler extends AbstractTaskEventHandler
{
    public function __invoke(TaskOpenedEvent $event): void
    {
        $this->logger->info(sprintf('Task "%s" has been successfully opened', $event->taskId->toString()));
    }
}
