<?php

declare(strict_types=1);

namespace App\Task\Application\Task\EventHandler;

use App\Task\Domain\Task\Event\TaskCanceledEvent;

final class TaskCanceledEventHandler extends AbstractTaskEventHandler
{
    public function __invoke(TaskCanceledEvent $event): void
    {
        $this->logger->info(sprintf('Task "%s" has been successfully canceled', $event->taskId->toString()));
    }
}
