<?php

declare(strict_types=1);

namespace App\Task\Application\Task\EventHandler;

use App\Task\Domain\Task\Event\TaskClosedEvent;

final class TaskClosedEventHandler extends AbstractTaskEventHandler
{
    public function __invoke(TaskClosedEvent $event): void
    {
        $this->logger->info(sprintf('Task "%s" has been successfully closed', $event->taskId->toString()));
    }
}
