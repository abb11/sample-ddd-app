<?php

declare(strict_types=1);

namespace App\Task\Application\Task\EventHandler;

use Psr\Log\LoggerInterface;

abstract class AbstractTaskEventHandler
{
    public function __construct(
        protected readonly LoggerInterface $logger,
    ) {
    }
}
