<?php

declare(strict_types=1);

namespace App\Task\Application\Task\EventHandler;

use App\Task\Domain\Task\Event\TaskCreatedEvent;

final class TaskCreatedEventHandler extends AbstractTaskEventHandler
{
    public function __invoke(TaskCreatedEvent $event): void
    {
        $this->logger->info(sprintf('Task "%s" has been successfully created', $event->taskId->toString()));
    }
}
