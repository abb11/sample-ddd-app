<?php

declare(strict_types=1);

namespace App\Task\Application\Task\EventHandler;

use App\Task\Domain\Task\Event\TaskUpdatedEvent;

final class TaskUpdatedEventHandler extends AbstractTaskEventHandler
{
    public function __invoke(TaskUpdatedEvent $event): void
    {
        $this->logger->info(sprintf('Task "%s" has been successfully updated', $event->taskId->toString()));
    }
}
