<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Exception;

use App\Shared\Domain\Exception\BusinessRuleViolationException;

/**
 * This exception is thrown when task is not open
 */
class TaskNotOpenException extends BusinessRuleViolationException
{
    private string $taskId;

    /**
     * Constructor
     *
     * @param string $taskId Task ID
     */
    public function __construct(string $taskId)
    {
        parent::__construct(sprintf('Task with ID "%s" is not open', $taskId));

        $this->taskId = $taskId;
    }

    /**
     * Returns task ID
     */
    public function getTaskId(): string
    {
        return $this->taskId;
    }
}
