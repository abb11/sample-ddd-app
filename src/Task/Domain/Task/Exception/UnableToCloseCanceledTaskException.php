<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Exception;

use App\Shared\Domain\Exception\BusinessRuleViolationException;

class UnableToCloseCanceledTaskException extends BusinessRuleViolationException
{
    public function __construct()
    {
        parent::__construct('Unable to close canceled task');
    }
}
