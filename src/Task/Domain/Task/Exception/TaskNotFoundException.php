<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Exception;

use App\System\ValueObject\Uuid\Uuid;

/**
 * This exception is thrown when task does not exist
 */
class TaskNotFoundException extends \Exception
{
    private Uuid $taskId;

    /**
     * Constructor
     *
     * @param Uuid $taskId Task ID
     */
    public function __construct(Uuid $taskId)
    {
        parent::__construct(sprintf('Task with ID "%s" does not exist', $taskId->toString()));

        $this->taskId = $taskId;
    }

    /**
     * Returns task ID
     */
    public function getTaskId(): Uuid
    {
        return $this->taskId;
    }
}
