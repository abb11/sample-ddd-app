<?php

declare(strict_types=1);

namespace App\Task\Domain\Task;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;

final class TaskFactory
{
    public function __construct(
        private readonly TaskOutsideInterface $outside,
    ) {
    }

    public function create(
        Uuid $id,
        string $title,
        string $priority,
        ?Date $executionDay = null,
    ): Task {
        return new Task(
            $this->outside,
            $id,
            $title,
            $priority,
            $executionDay,
        );
    }
}
