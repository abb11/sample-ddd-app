<?php

declare(strict_types=1);

namespace App\Task\Domain\Task;

use App\Task\Domain\Task\Event\TaskEventInterface;

interface TaskOutsideInterface
{
    public function publishEvent(TaskEventInterface $event): void;

    public function getCurrentTime(): \DateTimeImmutable;
}
