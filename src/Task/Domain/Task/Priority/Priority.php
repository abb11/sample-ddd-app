<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Priority;

use App\Task\Domain\Task\Priority\Exception\InvalidPriorityException;

/**
 * Priority
 */
class Priority
{
    public const LOW = 'low';
    public const NORMAL = 'normal';
    public const HIGH = 'high';

    private string $value;

    public function __construct(string $value)
    {
        if (!in_array($value, [self::LOW, self::NORMAL, self::HIGH])) {
            throw new InvalidPriorityException($value);
        }

        $this->value = $value;
    }

    /**
     * Change priority to given value
     */
    public function change(string $newPriority): Priority
    {
        if ($newPriority === $this->value) {
            return $this;
        }

        return new self($newPriority);
    }

    /**
     * Returns current priority value
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
