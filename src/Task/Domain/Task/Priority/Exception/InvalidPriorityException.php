<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Priority\Exception;

use App\Shared\Domain\Exception\BusinessRuleViolationException;

/**
 * This exception is thrown when invalid priority is given
 */
class InvalidPriorityException extends BusinessRuleViolationException
{
    public function __construct(string $priority)
    {
        parent::__construct(sprintf('Priority "%s" is not valid', $priority));
    }
}
