<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Event;

use App\System\ValueObject\Uuid\Uuid;

final class TaskOpenedEvent implements TaskEventInterface
{
    public function __construct(
        public readonly Uuid $taskId,
    ) {
    }

    public function getTaskId(): Uuid
    {
        return $this->taskId;
    }
}
