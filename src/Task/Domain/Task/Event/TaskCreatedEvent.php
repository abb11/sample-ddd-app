<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Event;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;

final class TaskCreatedEvent implements TaskEventInterface
{
    public function __construct(
        public readonly Uuid $taskId,
        public readonly string $title,
        public readonly string $status,
        public readonly string $priority,
        public readonly ?Date $executionDay,
        public readonly \DateTimeImmutable $createdAt,
    ) {
    }

    public function getTaskId(): Uuid
    {
        return $this->taskId;
    }
}
