<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Event;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;

final class TaskUpdatedEvent implements TaskEventInterface
{
    public function __construct(
        public readonly Uuid $taskId,
        public readonly string $newTitle,
        public readonly string $newPriority,
        public readonly ?Date $newExecutionDay,
    ) {
    }

    public function getTaskId(): Uuid
    {
        return $this->taskId;
    }
}
