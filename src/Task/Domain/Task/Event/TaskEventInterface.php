<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Event;

use App\Shared\Domain\Event\DomainEventInterface;
use App\System\ValueObject\Uuid\Uuid;

interface TaskEventInterface extends DomainEventInterface
{
    public function getTaskId(): Uuid;
}
