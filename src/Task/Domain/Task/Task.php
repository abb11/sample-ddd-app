<?php

declare(strict_types=1);

namespace App\Task\Domain\Task;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Domain\Task\Event\TaskCanceledEvent;
use App\Task\Domain\Task\Event\TaskClosedEvent;
use App\Task\Domain\Task\Event\TaskCreatedEvent;
use App\Task\Domain\Task\Event\TaskOpenedEvent;
use App\Task\Domain\Task\Event\TaskUpdatedEvent;
use App\Task\Domain\Task\Exception\TaskNotOpenException;
use App\Task\Domain\Task\Exception\UnableToCancelClosedTaskException;
use App\Task\Domain\Task\Exception\UnableToCloseCanceledTaskException;
use App\Task\Domain\Task\Exception\UnableToOpenCanceledTaskException;
use App\Task\Domain\Task\Priority\Priority;
use App\Task\Domain\Task\Status\Status;
use App\Task\Domain\Task\Title\Title;

/**
 * Task aggregate root
 */
class Task
{
    private TaskOutsideInterface $outside;

    private Uuid $id;

    private Title $title;

    private Priority $priority;

    private Status $status;

    private ?Date $executionDay;

    private \DateTimeImmutable $createdAt;

    public function __construct(
        TaskOutsideInterface $outside,
        Uuid $id,
        string $title,
        string $priority,
        ?Date $executionDay
    ) {
        $this->outside = $outside;
        $this->id = $id;
        $this->title = new Title($title);
        $this->priority = new Priority($priority);
        $this->status = new Status(Status::OPEN);
        $this->executionDay = $executionDay;
        $this->createdAt = $this->outside->getCurrentTime();

        $this->outside->publishEvent(new TaskCreatedEvent(
            $id,
            $title,
            Status::OPEN,
            $priority,
            $executionDay,
            $this->createdAt
        ));
    }

    /**
     * Updates task's params
     */
    public function update(string $newTitle, string $newPriority, ?Date $newExecutionDay): void
    {
        $this->assertIsOpen();

        $this->title = $this->title->change($newTitle);
        $this->priority = $this->priority->change($newPriority);

        if (null === $newExecutionDay || null === $this->executionDay) {
            $this->executionDay = $newExecutionDay;
        } else {
            $this->executionDay = $this->executionDay->change($newExecutionDay->toString());
        }

        $this->outside->publishEvent(new TaskUpdatedEvent($this->id, $newTitle, $newPriority, $newExecutionDay));
    }

    /**
     * Open task
     */
    public function open(): void
    {
        if ($this->status->is(Status::OPEN)) {
            return;
        }

        if ($this->status->is(Status::CANCELED)) {
            throw new UnableToOpenCanceledTaskException();
        }

        $this->status = $this->status->change(Status::OPEN);

        $this->outside->publishEvent(new TaskOpenedEvent($this->id));
    }

    /**
     * Close task
     */
    public function close(): void
    {
        if ($this->status->is(Status::CLOSED)) {
            return;
        }

        if ($this->status->is(Status::CANCELED)) {
            throw new UnableToCloseCanceledTaskException();
        }

        $this->status = $this->status->change(Status::CLOSED);

        $this->outside->publishEvent(new TaskClosedEvent($this->id));
    }

    /**
     * Cancel task
     */
    public function cancel(): void
    {
        if ($this->status->is(Status::CANCELED)) {
            return;
        }

        if ($this->status->is(Status::CLOSED)) {
            throw new UnableToCancelClosedTaskException();
        }

        $this->status = $this->status->change(Status::CANCELED);

        $this->outside->publishEvent(new TaskCanceledEvent($this->id));
    }

    /**
     * Assert that task is open
     */
    private function assertIsOpen(): void
    {
        if (!$this->status->is(Status::OPEN)) {
            throw new TaskNotOpenException($this->id->toString());
        }
    }
}
