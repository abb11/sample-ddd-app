<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Status\Exception;

use App\Shared\Domain\Exception\BusinessRuleViolationException;

/**
 * This exception is thrown when invalid status is given
 */
class InvalidStatusException extends BusinessRuleViolationException
{
    public function __construct(string $status)
    {
        parent::__construct(sprintf('Status "%s" is not valid', $status));
    }
}
