<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Status;

use App\Task\Domain\Task\Status\Exception\InvalidStatusException;

/**
 * Task status
 */
class Status
{
    public const OPEN = 'open';
    public const CLOSED = 'closed';
    public const CANCELED = 'canceled';

    private string $value;

    public function __construct(string $value)
    {
        if (!in_array($value, [self::OPEN, self::CLOSED, self::CANCELED])) {
            throw new InvalidStatusException($value);
        }

        $this->value = $value;
    }

    /**
     * Changes status to given value
     */
    public function change(string $newStatus): Status
    {
        if ($newStatus === $this->value) {
            return $this;
        }

        return new self($newStatus);
    }

    /**
     * Checks current status
     */
    public function is(string $status): bool
    {
        return $this->value === $status;
    }

    /**
     * Returns current status value
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
