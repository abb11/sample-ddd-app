<?php

declare(strict_types=1);

namespace App\Task\Domain\Task;

use App\System\ValueObject\Uuid\Uuid;
use App\Task\Domain\Task\Exception\TaskNotFoundException;

/**
 * Task repository interface
 */
interface TaskRepositoryInterface
{
    /**
     * Get task by ID
     *
     * @param Uuid $id Task ID
     *
     * @throws TaskNotFoundException
     */
    public function get(Uuid $id): Task;

    /**
     * Add task
     */
    public function add(Task $task): void;
}
