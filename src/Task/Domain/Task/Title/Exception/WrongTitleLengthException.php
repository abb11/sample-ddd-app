<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Title\Exception;

use App\Shared\Domain\Exception\BusinessRuleViolationException;

/**
 * This exception is thrown when a title with invalid length is given
 */
class WrongTitleLengthException extends BusinessRuleViolationException
{
    private int $minLength;

    private int $maxLength;

    private int $titleLength;

    /**
     * Constructor
     *
     * @param int $minLength   Min length
     * @param int $maxLength   Max length
     * @param int $titleLength Title length
     */
    public function __construct(int $minLength, int $maxLength, int $titleLength)
    {
        parent::__construct(sprintf(
            'Title should have from %s to %s characters. Got %s.',
            $minLength,
            $maxLength,
            $titleLength
        ));

        $this->minLength = $maxLength;
        $this->maxLength = $maxLength;
        $this->titleLength = $titleLength;
    }

    /**
     * Returns min length
     */
    public function getMinLength(): int
    {
        return $this->minLength;
    }

    /**
     * Returns max length
     */
    public function getMaxLength(): int
    {
        return $this->maxLength;
    }

    /**
     * Returns title length
     */
    public function getTitleLength(): int
    {
        return $this->titleLength;
    }
}
