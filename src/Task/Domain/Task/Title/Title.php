<?php

declare(strict_types=1);

namespace App\Task\Domain\Task\Title;

use App\Task\Domain\Task\Title\Exception\WrongTitleLengthException;

/**
 * Title
 */
class Title
{
    public const MIN_LENGTH = 3;
    public const MAX_LENGTH = 100;

    private string $value;

    /**
     * Constructor
     *
     * @param string $value Title
     */
    public function __construct(string $value)
    {
        $titleLength = mb_strlen($value);

        if ($titleLength < self::MIN_LENGTH || $titleLength > self::MAX_LENGTH) {
            throw new WrongTitleLengthException(self::MIN_LENGTH, self::MAX_LENGTH, $titleLength);
        }

        $this->value = $value;
    }

    /**
     * Change title to given value
     *
     * @param string $newTitle New title
     */
    public function change(string $newTitle): Title
    {
        if ($newTitle === $this->value) {
            return $this;
        }

        return new self($newTitle);
    }

    /**
     * Returns title value
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
