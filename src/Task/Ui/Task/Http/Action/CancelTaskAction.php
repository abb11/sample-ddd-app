<?php

declare(strict_types=1);

namespace App\Task\Ui\Task\Http\Action;

use App\Shared\Ui\Http\Action\AbstractHttpAction;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Command\Cancel\CancelTaskCommand;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CancelTaskAction extends AbstractHttpAction
{
    #[Route('/api/v1/tasks/{taskId}/cancel', methods: ['PATCH'])]
    public function __invoke(Uuid $taskId): Response
    {
        $this->executeCommand(new CancelTaskCommand($taskId));

        return $this->createResponse('', Response::HTTP_NO_CONTENT);
    }
}
