<?php

declare(strict_types=1);

namespace App\Task\Ui\Task\Http\Action;

use App\Shared\Ui\Http\Action\AbstractHttpAction;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Query\Get\GetTaskQuery;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class GetTaskAction extends AbstractHttpAction
{
    #[Route('/api/v1/tasks/{taskId}', methods: ['GET'])]
    public function __invoke(Uuid $taskId): Response
    {
        return $this->createResponse(
            $this->executeQuery(new GetTaskQuery($taskId))
        );
    }
}
