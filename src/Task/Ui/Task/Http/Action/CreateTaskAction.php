<?php

declare(strict_types=1);

namespace App\Task\Ui\Task\Http\Action;

use App\Shared\Ui\Http\Action\AbstractHttpAction;
use App\Task\Application\Task\Command\Create\CreateTaskCommand;
use App\Task\Ui\Task\Http\Input\CreateTaskInput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreateTaskAction extends AbstractHttpAction
{
    #[Route('/api/v1/tasks', methods: ['POST'])]
    public function __invoke(CreateTaskInput $input): Response
    {
        $taskId = $this->getUuid();
        $this->executeCommand(new CreateTaskCommand(
            $taskId,
            $input->getTitle(),
            $input->getPriority(),
            $input->getExecutionDay()
        ));

        return $this->createResponse(['id' => $taskId->toString()], Response::HTTP_CREATED);
    }
}
