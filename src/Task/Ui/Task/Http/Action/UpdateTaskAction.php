<?php

declare(strict_types=1);

namespace App\Task\Ui\Task\Http\Action;

use App\Shared\Ui\Http\Action\AbstractHttpAction;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Command\Update\UpdateTaskCommand;
use App\Task\Ui\Task\Http\Input\UpdateTaskInput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class UpdateTaskAction extends AbstractHttpAction
{
    #[Route('/api/v1/tasks/{taskId}', methods: ['PUT'])]
    public function __invoke(Uuid $taskId, UpdateTaskInput $input): Response
    {
        $this->executeCommand(new UpdateTaskCommand(
            $taskId,
            $input->getTitle(),
            $input->getPriority(),
            $input->getExecutionDay(),
        ));

        return $this->createResponse('', Response::HTTP_NO_CONTENT);
    }
}
