<?php

declare(strict_types=1);

namespace App\Task\Ui\Task\Http\Action;

use App\Shared\Ui\Http\Action\AbstractHttpAction;
use App\Task\Application\Task\Projection\TaskFilter;
use App\Task\Application\Task\Query\Find\FindTasksQuery;
use App\Task\Ui\Task\Http\Input\FindTasksInput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class FindTasksAction extends AbstractHttpAction
{
    private readonly int $tasksPerPageLimit;

    public function __construct(int $paginationPerPageLimit)
    {
        $this->tasksPerPageLimit = $paginationPerPageLimit;
    }

    #[Route('/api/v1/tasks', methods: ['GET'])]
    public function __invoke(FindTasksInput $input): Response
    {
        $filter = TaskFilter::create()
            ->setExecutionDay($input->getExecutionDay())
            ->setSortBy($input->getSortBy())
            ->setSortDir($input->getSortDir())
            ->setPerPageLimit($this->tasksPerPageLimit)
            ->setPage($input->getPage());

        $result = $this->executeQuery(new FindTasksQuery($filter));

        return $this->createResponse($result);
    }
}
