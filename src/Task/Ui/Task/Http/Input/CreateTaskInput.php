<?php

declare(strict_types=1);

namespace App\Task\Ui\Task\Http\Input;

use App\System\Request\AbstractInput;
use App\System\ValueObject\Date\Date;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateTaskInput extends AbstractInput
{
    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'fields' => [
                'title' => [
                    new Assert\NotBlank(),
                    new Assert\Type('string'),
                ],
                'priority' => [
                    new Assert\NotBlank(),
                    new Assert\Type('string'),
                ],
                'executionDay' => [
                    new Assert\Sequentially([
                        new Assert\NotBlank(['allowNull' => true]),
                        new Assert\Type('string'),
                        new Assert\Date(),
                    ]),
                ],
            ],
        ]);
    }

    public function getTitle(): string
    {
        return $this->data->get('title');
    }

    public function getPriority(): string
    {
        return $this->data->get('priority');
    }

    public function getExecutionDay(): ?Date
    {
        return null !== $this->data->get('executionDay')
            ? new Date($this->data->get('executionDay'))
            : null;
    }
}
