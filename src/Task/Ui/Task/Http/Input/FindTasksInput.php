<?php

declare(strict_types=1);

namespace App\Task\Ui\Task\Http\Input;

use App\System\Request\AbstractInput;
use App\System\Validator\Constraints\PageRequirements;
use App\System\Validator\Constraints\SortByRequirements;
use App\System\Validator\Constraints\SortDirRequirements;
use App\System\ValueObject\Date\Date;
use App\Task\Application\Task\Projection\TaskFilter;
use Symfony\Component\Validator\Constraints as Assert;

final class FindTasksInput extends AbstractInput
{
    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'fields' => [
                'executionDay' => new Assert\Optional([
                    new Assert\NotBlank(),
                    new Assert\Date(),
                ]),
                'page' => new Assert\Optional([
                    new PageRequirements(),
                ]),
                'sortBy' => new Assert\Optional([
                    new SortByRequirements([
                        'allowedSortCols' => TaskFilter::getAllowedSortCols(),
                    ]),
                ]),
                'sortDir' => new Assert\Optional([
                    new SortDirRequirements(),
                ]),
            ],
        ]);
    }

    public function getExecutionDay(): ?Date
    {
        return null !== $this->data->get('executionDay')
            ? new Date($this->data->get('executionDay'))
            : null;
    }

    public function getPage(): int
    {
        return (int) $this->data->get('page', 1);
    }

    public function getSortBy(): ?string
    {
        return $this->data->get('sortBy');
    }

    public function getSortDir(): ?string
    {
        return $this->data->get('sortDir');
    }
}
