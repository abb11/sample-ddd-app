<?php

declare(strict_types=1);

namespace App\Shared\Domain\Exception;

/**
 * Base exception for business rule violation
 */
abstract class BusinessRuleViolationException extends \LogicException
{
}
