<?php

declare(strict_types=1);

namespace App\Shared\Ui\Cli;

use Psr\Log\LogLevel;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'app:shared:lint-exceptions-mapping',
    description: 'Validates exceptions mapping config'
)]
final class ExceptionsMappingLintCommand extends Command
{
    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly array $exceptionToHttpResponseMapping,
        private readonly array $exceptionToLogLevelMapping
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        foreach ($this->exceptionToHttpResponseMapping as $class => $config) {
            if (!$this->exceptionExists($class)) {
                $io->error(sprintf('Class "%s" does not exist or is not an Exception class.', $class));

                return Command::FAILURE;
            }

            if (empty($config)) {
                $io->error(sprintf('Missing config for "%s".', $class));

                return Command::FAILURE;
            }

            $violations = $this->validator->validate($config, $this->getConfigConstraints());

            if ($violations->count() > 0) {
                $errors = $this->getErrorMessages($violations);
                $io->error([
                    sprintf('HTTP response mapping for "%s" has following errors:', $class),
                    ...$errors,
                ]);

                return Command::FAILURE;
            }
        }

        foreach ($this->exceptionToLogLevelMapping as $class => $logLevel) {
            if (!$this->exceptionExists($class)) {
                $io->error(sprintf('Class "%s" does not exist or is not an Exception class.', $class));

                return Command::FAILURE;
            }

            $violations = $this->validator->validate($logLevel, $this->getLogLevelConstraints());

            if ($violations->count() > 0) {
                $errors = $this->getErrorMessages($violations);
                $io->error([
                    sprintf('Log level mapping for "%s" has following errors:', $class),
                    ...$errors,
                ]);

                return Command::FAILURE;
            }
        }

        $io->success('The exceptions mapping is valid.');

        return Command::SUCCESS;
    }

    private function exceptionExists(string $class): bool
    {
        return class_exists($class) && is_a($class, \Exception::class, true);
    }

    private function getConfigConstraints(): Assert\Collection
    {
        $fields = [
            'status_code' => [
                new Assert\NotNull(),
                new Assert\Choice([
                    'choices' => array_keys(Response::$statusTexts),
                    'message' => 'Value "{{ value }}" is not a valid HTTP status code.',
                ]),
            ],
            'message' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ],
        ];

        return new Assert\Collection([
            'allowMissingFields' => true,
            'allowExtraFields' => false,
            'extraFieldsMessage' => sprintf(
                'This option is not allowed. Allowed options are: %s.',
                implode(', ', array_keys($fields))
            ),
            'fields' => $fields,
        ]);
    }

    private function getLogLevelConstraints(): array
    {
        return [
            new Assert\NotNull(),
            new Assert\Choice([
                'choices' => [
                    LogLevel::EMERGENCY,
                    LogLevel::ALERT,
                    LogLevel::CRITICAL,
                    LogLevel::ERROR,
                    LogLevel::WARNING,
                    LogLevel::NOTICE,
                    LogLevel::INFO,
                    LogLevel::DEBUG,
                ],
                'message' => 'Value {{ value }} is not a valid PSR-3 log level.',
            ]),
        ];
    }

    private function getErrorMessages(ConstraintViolationList $violations): array
    {
        $errors = [];

        foreach ($violations as $violation) {
            if ('' !== $violation->getPropertyPath()) {
                $errors[] = sprintf('%s: %s', trim($violation->getPropertyPath(), '[]'), $violation->getMessage());
            } else {
                $errors[] = $violation->getMessage();
            }
        }

        return $errors;
    }
}
