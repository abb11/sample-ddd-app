<?php

declare(strict_types=1);

namespace App\Shared\Ui\Cli;

use App\Shared\Infrastructure\EventStore\EventStoreInterface;
use Grifix\Worker\AbstractWorkerCommand;
use Grifix\Worker\WorkerFactoryInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\DependencyInjection\ServicesResetter;

#[AsCommand(
    name: 'app:shared:run-event-publisher-worker',
    description: 'Sends all unpublished events to the message broker'
)]
final class RunEventPublisherWorkerCommand extends AbstractWorkerCommand
{
    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly ServicesResetter $servicesResetter,
        WorkerFactoryInterface $workerFactory,
    ) {
        parent::__construct($workerFactory);
    }

    protected function getCallback(InputInterface $input, OutputInterface $output): callable
    {
        return function () {
            $this->eventStore->publishEvents();
            $this->servicesResetter->reset();
        };
    }
}
