<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\Action;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HealthCheckAction extends AbstractHttpAction
{
    #[Route('/', methods: ['GET'])]
    public function __invoke(): Response
    {
        return $this->createResponse(['status' => 'OK']);
    }
}
