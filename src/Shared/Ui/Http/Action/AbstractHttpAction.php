<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\Action;

use App\Shared\Application\Bus\CommandBusInterface;
use App\Shared\Application\Bus\QueryBusInterface;
use App\System\Enum\ContentType;
use App\System\Request\PayloadResolverInterface;
use App\System\Response\ResponseFactory;
use App\System\Utils\ArrayAccessor;
use App\System\ValueObject\Uuid\Uuid;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

/**
 * Base class for HTTP actions
 */
abstract class AbstractHttpAction implements ServiceSubscriberInterface
{
    #[Required]
    public ContainerInterface $container;

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedServices(): array
    {
        return [
            CommandBusInterface::class,
            QueryBusInterface::class,
            SerializerInterface::class,
            PayloadResolverInterface::class,
            ValidatorInterface::class,
            RequestStack::class,
            ResponseFactory::class,
        ];
    }

    /**
     * Executes given command.
     */
    protected function executeCommand(object $command): void
    {
        $this->container->get(CommandBusInterface::class)->executeCommand($command);
    }

    /**
     * Executes given query.
     */
    protected function executeQuery(object $query): mixed
    {
        return $this->container->get(QueryBusInterface::class)->executeQuery($query);
    }

    /**
     * Returns random UUID.
     */
    protected function getUuid(): Uuid
    {
        return Uuid::random();
    }

    /**
     * Returns a Response that uses the serializer component.
     */
    protected function createResponse(mixed $data, int $status = 200, array $headers = [], array $context = []): Response
    {
        return $this->container->get(ResponseFactory::class)->createResponse(
            $this->resolveContentType(),
            $data,
            $status,
            $headers,
            $context,
        );
    }

    /**
     * Resolves request payload.
     */
    protected function resolvePayload(Request $request): ArrayAccessor
    {
        return new ArrayAccessor(
            $this->container->get(PayloadResolverInterface::class)->resolvePayload($request)
        );
    }

    private function resolveContentType(): ContentType
    {
        $request = $this->container->get(RequestStack::class)->getMainRequest();

        return ContentType::Xml->value === $request->getContentTypeFormat() ? ContentType::Xml : ContentType::Json;
    }
}
