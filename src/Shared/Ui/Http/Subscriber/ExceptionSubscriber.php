<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\Subscriber;

use App\Shared\Ui\Http\ErrorRenderer\ErrorRendererInterface;
use App\System\Logger\ExceptionLoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ExceptionLoggerInterface $exceptionLogger,
        private readonly ErrorRendererInterface $errorRenderer,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['logKernelException', 0],
                ['handleKernelException', 0],
            ],
        ];
    }

    public function logKernelException(ExceptionEvent $event): void
    {
        $this->exceptionLogger->log($event->getThrowable());
    }

    public function handleKernelException(ExceptionEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $response = $this->errorRenderer->render($event->getThrowable(), $event->getRequest());

        $event->setResponse($response);
    }
}
