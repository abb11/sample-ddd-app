<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\ErrorRenderer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface ErrorRendererInterface
{
    /**
     * Renders an exception into HTTP response
     */
    public function render(\Throwable $exception, Request $request): Response;
}
