<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\ErrorRenderer;

use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\ExceptionMapperInterface;
use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\HttpError;
use App\System\Enum\ContentType;
use App\System\Helper\ExceptionHelper;
use App\System\Response\ResponseFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ErrorRenderer implements ErrorRendererInterface
{
    /**
     * @param iterable<ExceptionMapperInterface> $mappers
     */
    public function __construct(
        private readonly ResponseFactory $responseFactory,
        private readonly bool $debug = false,
        private readonly iterable $mappers = [],
        private readonly array $exceptionToHttpResponseMapping = [],
    ) {
    }

    public function render(\Throwable $exception, Request $request): Response
    {
        $httpError = $this->mapToHttpError($exception);

        $payload = [
            'error' => $httpError->payload,
        ];

        if ($this->debug) {
            $payload['debug'] = ExceptionHelper::convertToArray($exception, true);
        }

        $headers = array_merge(['Vary' => 'Accept'], $httpError->headers);

        return $this->responseFactory->createResponse(
            $this->resolveContentType($request),
            $payload,
            $httpError->status,
            $headers,
        );
    }

    private function mapToHttpError(\Throwable $exception): HttpError
    {
        $mapping = $this->getExceptionMapping($exception);

        foreach ($this->mappers as $mapper) {
            if (null !== $httpError = $mapper->map($exception, $mapping)) {
                return $httpError;
            }
        }

        $message = match (true) {
            isset($mapping['message']) => $mapping['message'],
            isset($mapping['status_code']) => $exception->getMessage(),
            default => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
        };

        $data = [
            'status' => $mapping['status_code'] ?? Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => $message,
        ];

        return new HttpError($data['status'], $data);
    }

    private function getExceptionMapping(\Throwable $exception): array
    {
        foreach ($this->exceptionToHttpResponseMapping as $class => $mapping) {
            if ($exception instanceof $class) {
                return $mapping;
            }
        }

        return [];
    }

    private function resolveContentType(Request $request): ContentType
    {
        return ContentType::Xml->value === $request->getContentTypeFormat() ? ContentType::Xml : ContentType::Json;
    }
}
