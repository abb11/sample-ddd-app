<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use Symfony\Component\HttpFoundation\Response;

final class RequestExceptionMapper implements ExceptionMapperInterface
{
    public function map(\Throwable $exception, array $context = []): ?HttpError
    {
        if (!$exception instanceof RequestExceptionInterface) {
            return null;
        }

        $payload = [
            'status' => $context['status_code'] ?? Response::HTTP_BAD_REQUEST,
            'message' => $context['message'] ?? $exception->getMessage(),
        ];

        return new HttpError($payload['status'], $payload);
    }
}
