<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

final class HttpExceptionMapper implements ExceptionMapperInterface
{
    public function map(\Throwable $exception, array $context = []): ?HttpError
    {
        if (!$exception instanceof HttpExceptionInterface) {
            return null;
        }

        $payload = [
            'status' => $context['status_code'] ?? $exception->getStatusCode(),
            'message' => $context['message'] ?? $exception->getMessage(),
        ];

        return new HttpError($payload['status'], $payload, $exception->getHeaders());
    }
}
