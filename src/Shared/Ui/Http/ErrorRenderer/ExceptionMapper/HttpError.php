<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

class HttpError
{
    public function __construct(
        public readonly int $status,
        public readonly array $payload,
        public readonly array $headers = [],
    ) {
    }
}
