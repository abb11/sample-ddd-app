<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

interface ExceptionMapperInterface
{
    /**
     * Map an exception to HTTP error. Return NULL if exception is not supported.
     */
    public function map(\Throwable $exception, array $context = []): ?HttpError;
}
