<?php

declare(strict_types=1);

namespace App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

use App\System\Exception\ValidationException;
use App\System\Helper\ConstraintViolationHelper;
use Symfony\Component\HttpFoundation\Response;

final class ValidationExceptionMapper implements ExceptionMapperInterface
{
    public function map(\Throwable $exception, array $context = []): ?HttpError
    {
        if (!$exception instanceof ValidationException) {
            return null;
        }

        $payload = [
            'status' => $context['status_code'] ?? Response::HTTP_BAD_REQUEST,
            'message' => $context['message'] ?? $exception->getMessage(),
            'details' => ConstraintViolationHelper::convertToArray($exception->getViolations()),
        ];

        return new HttpError($payload['status'], $payload);
    }
}
