<?php

declare(strict_types=1);

namespace App\Shared;

interface Constant
{
    public const PAGINATION_PER_PAGE_LIMIT = 10;
}
