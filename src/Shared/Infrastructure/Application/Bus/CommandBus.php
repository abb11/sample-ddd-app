<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Application\Bus;

use App\Shared\Application\Bus\CommandBusInterface;
use App\Shared\Infrastructure\EventStore\EventStoreInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

final class CommandBus implements CommandBusInterface
{
    use HandleTrait;

    public function __construct(
        MessageBusInterface $commandBus,
        private readonly EntityManagerInterface $entityManager,
        private readonly EventStoreInterface $eventStore,
    ) {
        $this->messageBus = $commandBus;
    }

    public function executeCommand(object $command): void
    {
        try {
            $this->handle($command);
        } catch (HandlerFailedException $e) {
            throw $e->getPrevious() ?? $e;
        }

        $this->entityManager->beginTransaction();

        try {
            $this->entityManager->flush();
            $this->eventStore->flush();
        } catch (\Throwable $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        $this->entityManager->commit();
        $this->entityManager->clear();
    }
}
