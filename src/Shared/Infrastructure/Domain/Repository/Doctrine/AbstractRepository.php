<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Domain\Repository\Doctrine;

use App\System\DependenciesInjector\EntityDependenciesInjectorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Psr\Container\ContainerInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

abstract class AbstractRepository implements ServiceSubscriberInterface
{
    public function __construct(
        protected readonly EntityManagerInterface $em,
        private readonly EntityDependenciesInjectorInterface $entityDependenciesInjector,
        private readonly ContainerInterface $entityDependenciesContainer,
    ) {
    }

    /**
     * In order to inject dependencies to your entity override the entityDependencies method and tag your repository:
     *
     * <pre>
     * use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
     * use Doctrine\ORM\Events;
     * ...
     *
     * #[AsEntityListener(event: Events::postLoad, method: 'injectDependencies', entity: YourEntity::class)]
     * </pre>
     */
    public function injectDependencies(object $entity, LifecycleEventArgs $event): void
    {
        if (!empty($this::entityDependencies())) {
            $this->entityDependenciesInjector->injectDependencies($entity, $this->entityDependenciesContainer);
        }
    }

    public static function getSubscribedServices(): array
    {
        return static::entityDependencies();
    }

    /**
     * @return array<class-string>
     */
    protected static function entityDependencies(): array
    {
        return [];
    }
}
