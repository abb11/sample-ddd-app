<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore;

use App\System\ValueObject\Uuid\Uuid;

final class EventEnvelope
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $publisherType,
        public readonly Uuid $publisherId,
        public readonly string $eventType,
        public readonly object $event,
        public readonly \DateTimeImmutable $createdAt,
        public readonly ?\DateTimeImmutable $publishedAt = null,
    ) {
    }
}
