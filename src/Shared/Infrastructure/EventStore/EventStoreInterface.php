<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore;

use App\System\ValueObject\Uuid\Uuid;

interface EventStoreInterface
{
    public function add(object $event, string $publisherType, Uuid $publisherId): void;

    public function flush(): void;

    public function publishEvents(): void;
}
