<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore\EventBus;

interface EventBusInterface
{
    public function dispatch(object $event): void;
}
