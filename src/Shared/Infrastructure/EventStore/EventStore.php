<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore;

use App\Shared\Infrastructure\EventStore\EventPublisher\EventPublisherInterface;
use App\Shared\Infrastructure\EventStore\Repository\EventRepositoryInterface;
use App\System\ValueObject\Uuid\Uuid;

final class EventStore implements EventStoreInterface
{
    public function __construct(
        private readonly EventRepositoryInterface $eventRepository,
        private readonly EventPublisherInterface $eventPublisher,
    ) {
    }

    public function add(object $event, string $publisherType, Uuid $publisherId): void
    {
        $this->eventRepository->add($event, $publisherType, $publisherId);
    }

    public function flush(): void
    {
        $this->eventRepository->flush();
    }

    public function publishEvents(): void
    {
        $this->eventPublisher->publishEvents();
    }
}
