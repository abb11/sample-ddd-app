<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore\Repository;

use App\Shared\Infrastructure\EventStore\EventEnvelope;
use App\System\ValueObject\Uuid\Uuid;

interface EventRepositoryInterface
{
    /**
     * @param string $publisherType FQN of event publisher
     */
    public function add(object $event, string $publisherType, Uuid $publisherId): void;

    public function flush(): void;

    public function markAsPublished(Uuid $eventId): void;

    /**
     * @return iterable<EventEnvelope>
     */
    public function find(?bool $published = null, ?int $limit = null, int $chunkSize = 100): iterable;
}
