<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore\Repository;

use App\Shared\Infrastructure\EventStore\EventEnvelope;
use App\System\ValueObject\Uuid\Uuid;
use Doctrine\DBAL\Connection;
use Grifix\Clock\ClockInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class DbalEventRepository implements EventRepositoryInterface
{
    private const EVENTS_TABLE = 'public.event_store';

    private const DATE_FORMAT = 'Y-m-d H:i:s.u';

    /** @var EventEnvelope[] */
    private array $events = [];

    public function __construct(
        private readonly Connection $connection,
        private readonly SerializerInterface $serializer,
        private readonly ClockInterface $clock,
        private readonly string $eventsTable = self::EVENTS_TABLE,
    ) {
    }

    public function add(object $event, string $publisherType, Uuid $publisherId): void
    {
        $this->events[] = new EventEnvelope(
            Uuid::random(),
            $publisherType,
            $publisherId,
            $event::class,
            $event,
            $this->clock->getCurrentTime(),
        );
    }

    public function flush(): void
    {
        while (!empty($this->events)) {
            foreach ($this->events as $i => $envelope) {
                $this->insertEvent($envelope);
                unset($this->events[$i]);
            }
        }
    }

    public function markAsPublished(Uuid $eventId): void
    {
        $this->connection->update($this->eventsTable, [
            'published_at' => $this->clock->getCurrentTime()->format(self::DATE_FORMAT),
        ], [
            'id' => $eventId->toString(),
        ]);
    }

    public function find(?bool $published = null, ?int $limit = null, int $chunkSize = 1000): iterable
    {
        $qb = $this->connection->createQueryBuilder()
            ->select('*')
            ->from($this->eventsTable);

        if (null !== $published) {
            $qb->andWhere(sprintf('published_at %s', $published ? 'IS NOT NULL' : 'IS NULL'));
        }

        $qb->orderBy('created_at', 'ASC');

        $qb->setFirstResult(0);

        if (null !== $limit) {
            $qb->setMaxResults($limit);
            $rows = $qb->fetchAllAssociative();

            yield from $this->createEvents($rows);
        } else {
            $qb->setMaxResults($chunkSize);
            $rows = $qb->fetchAllAssociative();

            while ($rows) {
                yield from $this->createEvents($rows);

                $qb->setFirstResult($qb->getFirstResult() + $chunkSize);
                $rows = $qb->fetchAllAssociative();
            }
        }
    }

    private function insertEvent(EventEnvelope $envelope): void
    {
        $this->connection->insert($this->eventsTable, [
            'id' => $envelope->id->toString(),
            'publisher_type' => $envelope->publisherType,
            'publisher_id' => $envelope->publisherId->toString(),
            'event_type' => $envelope->eventType,
            'event_payload' => $this->serializer->serialize($envelope->event, 'json'),
            'created_at' => $envelope->createdAt->format(self::DATE_FORMAT),
        ]);
    }

    private function createEvents(array $rows): iterable
    {
        foreach ($rows as $row) {
            yield $this->createEvent($row);
        }
    }

    private function createEvent($row): EventEnvelope
    {
        return new EventEnvelope(
            Uuid::fromString($row['id']),
            $row['publisher_type'],
            Uuid::fromString($row['publisher_id']),
            $row['event_type'],
            $this->serializer->deserialize($row['event_payload'], $row['event_type'], 'json'),
            new \DateTimeImmutable($row['created_at']),
            null !== $row['published_at'] ? new \DateTimeImmutable($row['published_at']) : null,
        );
    }
}
