<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore\EventPublisher;

use App\Shared\Infrastructure\EventStore\EventBus\EventBusInterface;
use App\Shared\Infrastructure\EventStore\Repository\EventRepositoryInterface;

final class EventPublisher implements EventPublisherInterface
{
    public function __construct(
        private readonly EventRepositoryInterface $eventRepository,
        private readonly EventBusInterface $eventBus,
    ) {
    }

    public function publishEvents(): void
    {
        foreach ($this->eventRepository->find(published: false, chunkSize: 100) as $envelope) {
            $this->eventBus->dispatch($envelope->event);
            $this->eventRepository->markAsPublished($envelope->id);
        }
    }
}
