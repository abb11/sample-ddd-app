<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\EventStore\EventPublisher;

interface EventPublisherInterface
{
    public function publishEvents(): void;
}
