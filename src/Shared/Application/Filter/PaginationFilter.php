<?php

declare(strict_types=1);

namespace App\Shared\Application\Filter;

use App\Shared\Constant;
use App\System\Enum\SortDir;
use Webmozart\Assert\Assert;

/**
 * Base class for pagination filters
 */
abstract class PaginationFilter
{
    protected int $page = 1;

    protected int $perPageLimit = Constant::PAGINATION_PER_PAGE_LIMIT;

    protected ?string $sortBy = null;

    protected ?string $sortDir = null;

    final public function __construct()
    {
    }

    /**
     * Creates a filter instance
     */
    public static function create(): static
    {
        return new static();
    }

    /**
     * Returns allowed sort directions
     *
     * @return string[]
     */
    public static function getAllowedSortDir(): array
    {
        return SortDir::list();
    }

    /**
     * Returns allowed sort columns
     *
     * @return string[]
     */
    abstract public static function getAllowedSortCols(): array;

    public function setPage(int $page): static
    {
        Assert::positiveInteger($page);

        $this->page = $page;

        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPerPageLimit(int $perPageLimit): static
    {
        Assert::positiveInteger($perPageLimit);

        $this->perPageLimit = $perPageLimit;

        return $this;
    }

    public function getPerPageLimit(): int
    {
        return $this->perPageLimit;
    }

    public function getOffset(): int
    {
        return ($this->page - 1) * $this->perPageLimit;
    }

    public function setSortBy(?string $sortBy): static
    {
        Assert::nullOrInArray(
            $sortBy,
            static::getAllowedSortCols(),
            'Unsupported sort column. Expected one of: %2$s. Got: %s'
        );

        $this->sortBy = $sortBy;

        return $this;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }

    public function setSortDir(?string $sortDir): static
    {
        Assert::nullOrInArray(
            $sortDir,
            static::getAllowedSortDir(),
            'Invalid sort direction. Expected one of: %2$s. Got: %s'
        );

        $this->sortDir = $sortDir;

        return $this;
    }

    public function getSortDir(): ?string
    {
        return $this->sortDir;
    }
}
