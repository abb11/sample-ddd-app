<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus;

interface CommandBusInterface
{
    /**
     * @throws \Throwable
     */
    public function executeCommand(object $command): void;
}
