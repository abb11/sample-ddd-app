<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus;

interface QueryBusInterface
{
    /**
     * @throws \Throwable
     */
    public function executeQuery(object $query): mixed;
}
