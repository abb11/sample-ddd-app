<?php

declare(strict_types=1);

namespace App\System\ValueObject\Date;

use App\System\Helper\DateHelper;
use App\System\ValueObject\Date\Exception\InvalidDateException;

class Date
{
    public const FORMAT = 'Y-m-d';

    protected string $value;

    /**
     * @throws InvalidDateException
     */
    public function __construct(string $date)
    {
        $this->assertIsValid($date);

        $this->value = $date;
    }

    /**
     * Creates date from string
     *
     * @throws InvalidDateException
     */
    public static function fromString(string $date): self
    {
        return new self($date);
    }

    /**
     * Returns date as string
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * Returns date as string
     */
    public function toString(): string
    {
        return $this->value;
    }

    /**
     * Changes date
     */
    public function change(string $newDate): self
    {
        if ($this->value === $newDate) {
            return $this;
        }

        return new self($newDate);
    }

    /**
     * Is the date equal to the given one?
     */
    public function equals(Date $date): bool
    {
        return $this->value === $date->value;
    }

    /**
     * Is the date greater than the given one?
     */
    public function isGreaterThan(Date $date): bool
    {
        return strtotime($this->value) > strtotime($date->value);
    }

    /**
     * Is the date less than the given one?
     */
    public function isLessThan(Date $date): bool
    {
        return strtotime($this->value) < strtotime($date->value);
    }

    /**
     * @throws InvalidDateException
     */
    protected function assertIsValid(string $date): void
    {
        if (!DateHelper::isValidDateInFormat($date, self::FORMAT)) {
            throw new InvalidDateException($date);
        }
    }
}
