<?php

declare(strict_types=1);

namespace App\System\ValueObject\Date\Exception;

/**
 * This exception is thrown when an invalid date is given
 */
class InvalidDateException extends \InvalidArgumentException
{
    public function __construct(string $date)
    {
        parent::__construct(sprintf('The value "%s" is not a valid date.', $date));
    }
}
