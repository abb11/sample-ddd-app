<?php

declare(strict_types=1);

namespace App\System\ValueObject\Uuid;

use App\System\ValueObject\Uuid\Exception\InvalidUuidException;
use Symfony\Component\Uid\UuidV4;

class Uuid
{
    private UuidV4 $uuid;

    private function __construct(UuidV4 $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Creates a UUID from a string
     *
     * @throws InvalidUuidException
     */
    public static function fromString(string $uuid): self
    {
        try {
            return new self(UuidV4::fromString($uuid));
        } catch (\InvalidArgumentException $e) {
            throw new InvalidUuidException(sprintf('UUID "%s" is invalid.', $uuid), $e->getCode(), $e);
        }
    }

    /**
     * Creates a random UUID
     */
    public static function random(): self
    {
        return new self(new UuidV4());
    }

    /**
     * Returns the UUID as a string
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * Returns the UUID as a string
     */
    public function toString(): string
    {
        return $this->uuid->__toString();
    }

    /**
     * Is UUID equal to given one?
     */
    public function equals(Uuid $other): bool
    {
        return $this->uuid->equals($other->uuid);
    }
}
