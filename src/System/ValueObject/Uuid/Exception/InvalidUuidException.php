<?php

declare(strict_types=1);

namespace App\System\ValueObject\Uuid\Exception;

/**
 * This exception is thrown when an invalid UUID is given
 */
class InvalidUuidException extends \InvalidArgumentException
{
}
