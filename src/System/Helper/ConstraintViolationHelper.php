<?php

declare(strict_types=1);

namespace App\System\Helper;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintViolationHelper
{
    /**
     * Converts violations to array
     */
    public static function convertToArray(ConstraintViolationListInterface $violations): array
    {
        $errors = [];

        /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $field = self::convertPathToDotNotation($violation->getPropertyPath());
            $errors[$field][] = $violation->getMessage();
        }

        return $errors;
    }

    private static function convertPathToDotNotation(string $path): string
    {
        return str_replace(['[', ']'], ['.', ''], trim($path, '[]'));
    }
}
