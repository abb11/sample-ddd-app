<?php

declare(strict_types=1);

namespace App\System\Helper;

class DateHelper
{
    /**
     * Checks if a given date is valid in expected format
     */
    public static function isValidDateInFormat(string $date, string $format): bool
    {
        return ($d = \DateTime::createFromFormat($format, $date))
            && $d->format($format) === $date;
    }
}
