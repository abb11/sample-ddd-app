<?php

declare(strict_types=1);

namespace App\System\Helper;

class ExceptionHelper
{
    public static function convertToArray(\Throwable $exception, bool $withPrevious = false): array
    {
        $data = [
            'exception' => get_class($exception),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'code' => $exception->getCode(),
            'trace' => $exception->getTraceAsString(),
        ];

        if ($withPrevious && null !== $exception->getPrevious()) {
            $data['previous'] = self::convertToArray($exception->getPrevious(), $withPrevious);
        }

        return $data;
    }
}
