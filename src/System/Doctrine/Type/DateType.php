<?php

declare(strict_types=1);

namespace App\System\Doctrine\Type;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Date\Exception\InvalidDateException;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateType as DoctrineDateType;

/**
 * Field type mapping for the Doctrine Database Abstraction Layer (DBAL).
 *
 * Date fields will be stored as a string in the database and converted back to
 * the Date value object when querying.
 */
final class DateType extends DoctrineDateType
{
    public const NAME = 'app_date';

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     *
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Date
    {
        if (null === $value || $value instanceof Date) {
            return $value;
        }

        if (!is_string($value)) {
            throw ConversionException::conversionFailedInvalidType(
                $value,
                self::NAME,
                ['null', 'string', Date::class]
            );
        }

        try {
            return Date::fromString($value);
        } catch (InvalidDateException $e) {
            throw ConversionException::conversionFailedFormat(
                $value,
                self::NAME,
                $platform->getDateFormatString(),
                $e
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (null === $value || '' === $value) {
            return null;
        }

        if ($value instanceof Date) {
            return $value->toString();
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            self::NAME,
            ['null', Date::class]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
