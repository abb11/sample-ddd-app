<?php

declare(strict_types=1);

namespace App\System\Doctrine\Type;

use App\System\ValueObject\Uuid\Exception\InvalidUuidException;
use App\System\ValueObject\Uuid\Uuid;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\GuidType;

/**
 * Field type mapping for the Doctrine Database Abstraction Layer (DBAL).
 *
 * UUID fields will be stored as a string in the database and converted back to
 * the Uuid value object when querying.
 */
final class UuidType extends GuidType
{
    public const NAME = 'uuid';

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * {@inheritDoc}
     *
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Uuid
    {
        if ($value instanceof Uuid || null === $value) {
            return $value;
        }

        if (!is_string($value)) {
            throw ConversionException::conversionFailedInvalidType(
                $value,
                self::NAME,
                ['null', 'string', Uuid::class]
            );
        }

        try {
            return Uuid::fromString($value);
        } catch (InvalidUuidException $e) {
            throw ConversionException::conversionFailed($value, self::NAME, $e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value instanceof Uuid) {
            return $value->toString();
        }

        if (null === $value || '' === $value) {
            return null;
        }

        if (!is_string($value)) {
            throw ConversionException::conversionFailedInvalidType(
                $value,
                self::NAME,
                ['null', 'string', Uuid::class]
            );
        }

        try {
            return Uuid::fromString($value)->toString();
        } catch (InvalidUuidException $e) {
            throw ConversionException::conversionFailed($value, self::NAME, $e);
        }
    }
}
