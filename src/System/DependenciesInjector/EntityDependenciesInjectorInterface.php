<?php

declare(strict_types=1);

namespace App\System\DependenciesInjector;

use Psr\Container\ContainerInterface;

interface EntityDependenciesInjectorInterface
{
    /**
     * Inject dependencies to given entity
     */
    public function injectDependencies(object $entity, ContainerInterface $dependencies): void;
}
