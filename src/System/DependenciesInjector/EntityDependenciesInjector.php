<?php

declare(strict_types=1);

namespace App\System\DependenciesInjector;

use Psr\Container\ContainerInterface;

final class EntityDependenciesInjector implements EntityDependenciesInjectorInterface
{
    public function injectDependencies(object $entity, ContainerInterface $dependencies): void
    {
        $reflClass = new \ReflectionClass($entity);

        foreach ($reflClass->getProperties() as $property) {
            if ($this->isDependency($property, $dependencies)) {
                $dependency = $dependencies->get($property->getType()->getName());
                $property->setValue($entity, $dependency);
            } else {
                $value = $property->getValue($entity);

                if ($this->isCollection($value)) {
                    foreach ($value as $item) {
                        if ($this->isValueObject($item)) {
                            $this->injectDependencies($item, $dependencies);
                        }
                    }
                } elseif ($this->isValueObject($value)) {
                    $this->injectDependencies($value, $dependencies);
                }
            }
        }
    }

    private function isDependency(\ReflectionProperty $property, ContainerInterface $dependencies): bool
    {
        return $property->getType() instanceof \ReflectionNamedType
            && false === $property->getType()->isBuiltin()
            && $dependencies->has($property->getType()->getName());
    }

    private function isCollection(mixed $value): bool
    {
        return is_array($value) || $value instanceof \Traversable;
    }

    private function isValueObject(mixed $value): bool
    {
        return is_object($value)
            && (new \ReflectionClass($value))->isUserDefined();
    }
}
