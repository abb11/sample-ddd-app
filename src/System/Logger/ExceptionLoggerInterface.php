<?php

declare(strict_types=1);

namespace App\System\Logger;

use Psr\Log\LogLevel;

interface ExceptionLoggerInterface
{
    public function log(\Throwable $exception, string $level = LogLevel::ERROR): void;
}
