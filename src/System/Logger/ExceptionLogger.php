<?php

declare(strict_types=1);

namespace App\System\Logger;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

final class ExceptionLogger implements ExceptionLoggerInterface
{
    private const DEFAULT_LOG_LEVEL = LogLevel::ERROR;

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly array $exceptionToLogLevelMapping,
    ) {
    }

    /**
     * @param string|null $level Log level (Psr\Log\LogLevel)
     */
    public function log(\Throwable $exception, ?string $level = null): void
    {
        $level ??= $this->getExceptionLogLevel($exception);
        $this->logger->log($level, $exception->getMessage(), ['exception' => $exception]);
    }

    private function getExceptionLogLevel(\Throwable $exception): string
    {
        foreach ($this->exceptionToLogLevelMapping as $class => $logLevel) {
            if ($exception instanceof $class) {
                return $logLevel;
            }
        }

        return self::DEFAULT_LOG_LEVEL;
    }
}
