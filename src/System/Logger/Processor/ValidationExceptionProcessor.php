<?php

declare(strict_types=1);

namespace App\System\Logger\Processor;

use App\System\Exception\ValidationException;
use App\System\Helper\ConstraintViolationHelper;
use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;

/**
 * Adds validation errors to log record if ValidationException is thrown
 */
final class ValidationExceptionProcessor implements ProcessorInterface
{
    public function __invoke(LogRecord $record): LogRecord
    {
        if (
            isset($record->context['exception'])
            && $record->context['exception'] instanceof ValidationException
        ) {
            $record->extra['violations'] = ConstraintViolationHelper::convertToArray(
                $record->context['exception']->getViolations()
            );
        }

        return $record;
    }
}
