<?php

declare(strict_types=1);

namespace App\System\Serializer\Normalizer;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Date\Exception\InvalidDateException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class DateNormalizer implements NormalizerInterface, DenormalizerInterface, CacheableSupportsMethodInterface
{
    public function normalize(mixed $object, string $format = null, array $context = []): string
    {
        return $object->toString();
    }

    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Date;
    }

    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): Date
    {
        try {
            return Date::fromString($data);
        } catch (InvalidDateException $e) {
            throw new NotNormalizableValueException(
                'The data is not a valid date string representation.',
                $e->getCode(),
                $e
            );
        }
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_a($type, Date::class, true);
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
