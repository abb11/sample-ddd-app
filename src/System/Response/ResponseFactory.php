<?php

declare(strict_types=1);

namespace App\System\Response;

use App\System\Enum\ContentType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

final class ResponseFactory
{
    public function __construct(
        private readonly SerializerInterface $serializer,
    ) {
    }

    public function createResponse(
        ContentType $contentType,
        mixed $data,
        int $status = 200,
        array $headers = [],
        array $context = [],
    ): Response {
        switch ($contentType) {
            case ContentType::Xml:
                $serializedData = $this->serializer->serialize($data, 'xml', $context);
                $response = new Response($serializedData, $status, $headers);
                $response->headers->set('Content-Type', 'application/xml');
                break;
            case ContentType::Json:
                $context = array_merge([
                    'json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS,
                ], $context);
                $serializedData = $this->serializer->serialize($data, 'json', $context);
                $response = new JsonResponse($serializedData, $status, $headers, true);
                break;
            default:
                throw new \InvalidArgumentException(sprintf('Unsupported content type: "%s".', $contentType->value));
        }

        return $response;
    }
}
