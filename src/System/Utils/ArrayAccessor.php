<?php

declare(strict_types=1);

namespace App\System\Utils;

use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Array Accessor
 */
final class ArrayAccessor
{
    private PropertyAccessorInterface $propertyAccessor;

    private array $array;

    public function __construct(array $array)
    {
        $this->array = $array;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();
    }

    /**
     * Returns the value at the end of the key path of the array
     */
    public function get(string $path, mixed $defaultValue = null): mixed
    {
        if (!$this->has($path)) {
            return $defaultValue;
        }

        return $this->propertyAccessor->getValue($this->array, $this->convertPath($path));
    }

    /**
     * Checks if the array has a value at the end of the key path
     */
    public function has(string $path): bool
    {
        try {
            $this->propertyAccessor->getValue($this->array, $this->convertPath($path));
        } catch (AccessException $exception) {
            return false;
        }

        return true;
    }

    /**
     * Sets the value at the end of the key path of the array
     */
    public function set(string $path, mixed $value): void
    {
        $this->propertyAccessor->setValue($this->array, $this->convertPath($path), $value);
    }

    /**
     * Returns the array
     */
    public function getArray(): array
    {
        return $this->array;
    }

    /**
     * Converts path
     */
    private function convertPath(string $path): string
    {
        return '[' . str_replace('.', '][', $path) . ']';
    }
}
