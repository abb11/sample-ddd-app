<?php

declare(strict_types=1);

namespace App\System\Request;

use Symfony\Component\HttpFoundation\Request;

/**
 * Payload resolver
 */
class PayloadResolver implements PayloadResolverInterface
{
    /**
     * {@inheritDoc}
     */
    public function resolvePayload(Request $request): array
    {
        if (in_array($request->getMethod(), [Request::METHOD_POST, Request::METHOD_PUT, Request::METHOD_PATCH])) {
            return $request->toArray();
        }

        return $request->query->all();
    }
}
