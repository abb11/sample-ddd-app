<?php

declare(strict_types=1);

namespace App\System\Request;

use App\System\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\Request;

interface InputFactoryInterface
{
    /**
     * @throws JsonException
     * @throws ValidationException
     */
    public function createFromRequest(Request $request, string $inputClass): AbstractInput;

    /**
     * @throws ValidationException
     */
    public function createFromArray(array $data, string $inputClass): AbstractInput;
}
