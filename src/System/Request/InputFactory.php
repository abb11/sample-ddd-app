<?php

declare(strict_types=1);

namespace App\System\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class InputFactory implements InputFactoryInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly PayloadResolverInterface $payloadResolver,
    ) {
    }

    public function createFromRequest(Request $request, string $inputClass): AbstractInput
    {
        $data = $this->payloadResolver->resolvePayload($request);

        return $this->createFromArray($data, $inputClass);
    }

    public function createFromArray(array $data, string $inputClass): AbstractInput
    {
        if (!is_subclass_of($inputClass, AbstractInput::class)) {
            throw new \InvalidArgumentException(sprintf('Class %s should extend %s.', $inputClass, AbstractInput::class));
        }

        return $inputClass::create($this->validator, $data);
    }
}
