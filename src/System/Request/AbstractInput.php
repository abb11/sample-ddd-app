<?php

declare(strict_types=1);

namespace App\System\Request;

use App\System\Exception\ValidationException;
use App\System\Utils\ArrayAccessor;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractInput
{
    protected ArrayAccessor $data;

    /**
     * @throws ValidationException
     */
    public function __construct(
        protected readonly ValidatorInterface $validator,
        array $data,
    ) {
        $this->validate($data);
        $this->data = new ArrayAccessor($data);
    }

    /**
     * @throws ValidationException
     */
    public static function create(ValidatorInterface $validator, array $data): static
    {
        return new static($validator, $data);
    }

    /**
     * @throws ValidationException
     */
    protected function validate(array $data): void
    {
        $violations = $this->validator->validate($data, $this->constraints());

        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }
    }

    /**
     * Get the validation constraints
     *
     * @see https://symfony.com/doc/current/validation/raw_values.html
     */
    abstract protected function constraints(): Assert\Collection;
}
