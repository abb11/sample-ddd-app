<?php

declare(strict_types=1);

namespace App\System\Request;

use Symfony\Component\HttpFoundation\Request;

interface PayloadResolverInterface
{
    /**
     * Resolve payload from given request
     */
    public function resolvePayload(Request $request): array;
}
