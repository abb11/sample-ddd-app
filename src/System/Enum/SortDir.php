<?php

declare(strict_types=1);

namespace App\System\Enum;

enum SortDir: string
{
    case Asc = 'asc';
    case Desc = 'desc';

    /**
     * @return string[]
     */
    public static function list(): array
    {
        return array_map(fn ($case) => $case->value, self::cases());
    }
}
