<?php

declare(strict_types=1);

namespace App\System\Enum;

enum ContentType: string
{
    case Json = 'json';
    case Xml = 'xml';
}
