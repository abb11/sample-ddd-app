<?php

declare(strict_types=1);

namespace App\System\Validator\Constraints;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Compound;

final class SortByRequirements extends Compound
{
    public array $allowedSortCols = [];

    public function getRequiredOptions(): array
    {
        return ['allowedSortCols'];
    }

    public function getDefaultOption(): ?string
    {
        return 'allowedSortCols';
    }

    protected function getConstraints(array $options): array
    {
        $this->allowedSortCols = $options['allowedSortCols'];

        return [
            new Assert\Sequentially([
                new Assert\NotBlank(),
                new Assert\Choice([
                    'choices' => $this->allowedSortCols,
                    'message' => 'Sorting by {{ value }} column is not allowed.',
                ]),
            ]),
        ];
    }
}
