<?php

declare(strict_types=1);

namespace App\System\Validator\Constraints;

use App\System\Enum\SortDir;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Compound;

final class SortDirRequirements extends Compound
{
    public array $allowedSortDir = [];

    public function getDefaultOption(): ?string
    {
        return 'allowedSortDir';
    }

    protected function getConstraints(array $options): array
    {
        $this->allowedSortDir = $options['allowedSortDir'] ?? SortDir::list();

        return [
            new Assert\Sequentially([
                new Assert\NotBlank(),
                new Assert\Choice([
                    'choices' => $this->allowedSortDir,
                    'message' => 'The sort direction you choose is invalid.',
                ]),
            ]),
        ];
    }
}
