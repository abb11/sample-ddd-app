<?php

declare(strict_types=1);

namespace App\System\Validator\Constraints;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Compound;

/**
 * Page requirements
 */
final class PageRequirements extends Compound
{
    /**
     * {@inheritDoc}
     */
    protected function getConstraints(array $options): array
    {
        return [
            new Assert\Sequentially([
                new Assert\NotBlank(),
                new Assert\Type(['digit', 'int']),
                new Assert\Positive(),
            ]),
        ];
    }
}
