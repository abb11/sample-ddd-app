<?php

declare(strict_types=1);

namespace App\System\Validator;

use App\System\Exception\ValidationException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractValidator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Validate data
     *
     * @throws ValidationException
     */
    public function validate(array $data): void
    {
        $violations = $this->validator->validate($data, $this->constraints());

        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }
    }

    /**
     * Get the validation constraints
     *
     * @see https://symfony.com/doc/current/validation/raw_values.html
     */
    abstract protected function constraints(): Assert\Collection;
}
