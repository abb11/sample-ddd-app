<?php

declare(strict_types=1);

namespace App\System\Controller\ValueResolver;

use App\System\ValueObject\Uuid\Exception\InvalidUuidException;
use App\System\ValueObject\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class UuidValueResolver implements ValueResolverInterface
{
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$this->supports($request, $argument)) {
            return [];
        }

        /** @var class-string<Uuid> $uuidClass */
        $uuidClass = $argument->getType();

        try {
            yield $uuidClass::fromString($request->attributes->get($argument->getName()));
        } catch (InvalidUuidException $e) {
            throw new NotFoundHttpException(
                sprintf('The UUID for the "%s" parameter is invalid.', $argument->getName()),
                $e
            );
        }
    }

    private function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return !$argument->isVariadic()
            && Uuid::class === $argument->getType()
            && \is_string($request->attributes->get($argument->getName()));
    }
}
