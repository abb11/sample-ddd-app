<?php

declare(strict_types=1);

namespace App\System\Controller\ValueResolver;

use App\System\Request\AbstractInput;
use App\System\Request\InputFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class InputValueResolver implements ValueResolverInterface
{
    public function __construct(
        private readonly InputFactoryInterface $inputFactory,
    ) {
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$this->supports($argument)) {
            return [];
        }

        yield $this->inputFactory->createFromRequest($request, $argument->getType());
    }

    private function supports(ArgumentMetadata $argument): bool
    {
        return null !== $argument->getType()
            && !$argument->isVariadic()
            && is_subclass_of($argument->getType(), AbstractInput::class);
    }
}
