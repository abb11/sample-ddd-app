# Executables (local)
DOCKER_COMP = docker-compose

.DEFAULT_GOAL := help

.PHONY: help
help: ## Prints this help text
	@echo "Usage: make \033[32m<command>\033[0m\n"
	@echo "the following commands are available:\n"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## Docker

.PHONY: build
build: ## Build the Docker images
	@$(DOCKER_COMP) build --pull --no-cache

.PHONY: up
up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

.PHONY: down
down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

.PHONY: setup-app
setup-app: ## Install dependencies and execute database migrations
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php composer install
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php php bin/console doctrine:migrations:migrate --no-interaction

.PHONY: logs
logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

.PHONY: sh
sh: ## Connect to the PHP container
	@$(DOCKER_COMP) exec php bash

## Swagger

.PHONY: swagger-validate
swagger-validate: ## Validate OpenApi definition
	@$(DOCKER_COMP) exec swagger_ui swagger-cli validate /swagger/index.yaml

.PHONY: swagger-build
swagger-build: ## Bundle a multi-file OpenAPI definition into a single file (openapi.yaml)
	@$(DOCKER_COMP) exec swagger_ui sh /docker-entrypoint.d/50-build-openapi.sh

## Tests

.PHONY: init-test-db
init-test-db: ## Initialize database for tests
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php php bin/console -e test app:tests:init-db

.PHONY: tests
tests: ## Run all tests
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php php bin/phpunit

## Static code analyze

.PHONY: php-cs
php-cs: ## Lint PHP code
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php vendor/bin/php-cs-fixer fix --diff --dry-run --no-interaction -v

.PHONY: php-cs-fix
php-cs-fix: ## Lint and fix PHP code to follow the convention
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php vendor/bin/php-cs-fixer fix

.PHONY: phpstan
phpstan: ## Perform source code analysis
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php vendor/bin/phpstan analyse

## Events

.PHONY: run-event-publisher-worker
run-event-publisher: ## Sends all unpublished events to the message broker
	@$(DOCKER_COMP) exec -e XDEBUG_MODE=off php php bin/console app:shared:run-event-publisher-worker -vv
