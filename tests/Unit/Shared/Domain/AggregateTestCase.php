<?php

declare(strict_types=1);

namespace Tests\Unit\Shared\Domain;

use PHPUnit\Framework\TestCase;
use Tests\Utils\DomainEventCollector;

/**
 * Base class for test aggregates
 */
abstract class AggregateTestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        DomainEventCollector::instance()->clear();
    }

    protected function getDomainEvents(): array
    {
        return DomainEventCollector::instance()->get();
    }
}
