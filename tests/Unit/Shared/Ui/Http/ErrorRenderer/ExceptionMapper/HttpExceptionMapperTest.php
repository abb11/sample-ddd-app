<?php

declare(strict_types=1);

namespace Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\HttpError;
use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\HttpExceptionMapper;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\Dummy\TestHttpException;

final class HttpExceptionMapperTest extends TestCase
{
    private HttpExceptionMapper $mapper;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mapper = new HttpExceptionMapper();
    }

    public function testItShouldReturnNullForUnsupportedException(): void
    {
        $this->assertNull($this->mapper->map(new \Exception()));
    }

    /**
     * @dataProvider provideHttpExceptions
     */
    public function testItShouldMapHttpExceptionToHttpError(
        \Throwable $exception,
        array $context,
        int $expectedStatus,
        array $expectedPayload
    ): void {
        $httpError = $this->mapper->map($exception, $context);

        $this->assertInstanceOf(HttpError::class, $httpError);
        $this->assertSame($expectedStatus, $httpError->status);
        $this->assertSame($expectedPayload, $httpError->payload);
    }

    public function provideHttpExceptions(): iterable
    {
        yield 'with_empty_context' => [
            'exception' => new TestHttpException(500, 'Test message'),
            'context' => [],
            'expected_status' => 500,
            'expected_payload' => [
                'status' => 500,
                'message' => 'Test message',
            ],
        ];

        yield 'with_status_code' => [
            'exception' => new TestHttpException(500, 'Test message'),
            'context' => [
                'status_code' => 400,
            ],
            'expected_status' => 400,
            'expected_payload' => [
                'status' => 400,
                'message' => 'Test message',
            ],
        ];

        yield 'with_message' => [
            'exception' => new TestHttpException(422, 'Test message'),
            'context' => [
                'message' => 'Foo bar',
            ],
            'expected_status' => 422,
            'expected_payload' => [
                'status' => 422,
                'message' => 'Foo bar',
            ],
        ];

        yield 'with_status_code_and_message' => [
            'exception' => new TestHttpException(500, 'Test message'),
            'context' => [
                'status_code' => 400,
                'message' => 'Foo bar',
            ],
            'expected_status' => 400,
            'expected_payload' => [
                'status' => 400,
                'message' => 'Foo bar',
            ],
        ];
    }
}
