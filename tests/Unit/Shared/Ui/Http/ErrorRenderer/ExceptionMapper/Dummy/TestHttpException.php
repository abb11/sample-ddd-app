<?php

declare(strict_types=1);

namespace Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\Dummy;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class TestHttpException extends \Exception implements HttpExceptionInterface
{
    private int $statusCode;

    public function __construct(int $statusCode, string $message = '')
    {
        parent::__construct($message);

        $this->statusCode = $statusCode;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return [];
    }
}
