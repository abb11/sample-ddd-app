<?php

declare(strict_types=1);

namespace Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\Dummy;

use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;

class TestRequestException extends \Exception implements RequestExceptionInterface
{
}
