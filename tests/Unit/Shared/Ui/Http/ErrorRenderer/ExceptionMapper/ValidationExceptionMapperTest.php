<?php

declare(strict_types=1);

namespace Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\HttpError;
use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\ValidationExceptionMapper;
use App\System\Exception\ValidationException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

final class ValidationExceptionMapperTest extends TestCase
{
    private ValidationExceptionMapper $mapper;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mapper = new ValidationExceptionMapper();
    }

    public function testItShouldReturnNullForUnsupportedException(): void
    {
        $this->assertNull($this->mapper->map(new \Exception()));
    }

    /**
     * @dataProvider provideValidationExceptions
     */
    public function testItShouldMapValidationExceptionToHttpError(
        \Throwable $exception,
        array $context,
        int $expectedStatus,
        array $expectedPayload
    ): void {
        $httpError = $this->mapper->map($exception, $context);

        $this->assertInstanceOf(HttpError::class, $httpError);
        $this->assertSame($expectedStatus, $httpError->status);
        $this->assertSame($expectedPayload, $httpError->payload);
    }

    public function provideValidationExceptions(): iterable
    {
        $violations = new ConstraintViolationList();
        $violations->add(new ConstraintViolation('This value should not be blank.', '', [], null, '[task][date]', null));
        $validationException = new ValidationException($violations, 'Validation failed');
        $expectedErrors = [
            'task.date' => [
                'This value should not be blank.',
            ],
        ];

        yield 'with_empty_context' => [
            'exception' => $validationException,
            'context' => [],
            'expected_status' => 400,
            'expected_payload' => [
                'status' => 400,
                'message' => 'Validation failed',
                'details' => $expectedErrors,
            ],
        ];

        yield 'with_status_code' => [
            'exception' => $validationException,
            'context' => [
                'status_code' => 422,
            ],
            'expected_status' => 422,
            'expected_payload' => [
                'status' => 422,
                'message' => 'Validation failed',
                'details' => $expectedErrors,
            ],
        ];

        yield 'with_message' => [
            'exception' => $validationException,
            'context' => [
                'message' => 'Foo bar',
            ],
            'expected_status' => 400,
            'expected_payload' => [
                'status' => 400,
                'message' => 'Foo bar',
                'details' => $expectedErrors,
            ],
        ];

        yield 'with_status_code_and_message' => [
            'exception' => $validationException,
            'context' => [
                'status_code' => 422,
                'message' => 'Foo bar',
            ],
            'expected_status' => 422,
            'expected_payload' => [
                'status' => 422,
                'message' => 'Foo bar',
                'details' => $expectedErrors,
            ],
        ];
    }
}
