<?php

declare(strict_types=1);

namespace Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper;

use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\HttpError;
use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\RequestExceptionMapper;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\Dummy\TestRequestException;

final class RequestExceptionMapperTest extends TestCase
{
    private RequestExceptionMapper $mapper;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mapper = new RequestExceptionMapper();
    }

    public function testItShouldReturnNullForUnsupportedException(): void
    {
        $this->assertNull($this->mapper->map(new \Exception()));
    }

    /**
     * @dataProvider provideRequestExceptions
     */
    public function testItShouldMapRequestExceptionToHttpError(
        \Throwable $exception,
        array $context,
        int $expectedStatus,
        array $expectedPayload
    ): void {
        $httpError = $this->mapper->map($exception, $context);

        $this->assertInstanceOf(HttpError::class, $httpError);
        $this->assertSame($expectedStatus, $httpError->status);
        $this->assertSame($expectedPayload, $httpError->payload);
    }

    public function provideRequestExceptions(): iterable
    {
        yield 'with_empty_context' => [
            'exception' => new TestRequestException('Test message'),
            'context' => [],
            'expected_status' => 400,
            'expected_payload' => [
                'status' => 400,
                'message' => 'Test message',
            ],
        ];

        yield 'with_status_code' => [
            'exception' => new TestRequestException('Test message'),
            'context' => [
                'status_code' => 422,
            ],
            'expected_status' => 422,
            'expected_payload' => [
                'status' => 422,
                'message' => 'Test message',
            ],
        ];

        yield 'with_message' => [
            'exception' => new TestRequestException('Test message'),
            'context' => [
                'message' => 'Foo bar',
            ],
            'expected_status' => 400,
            'expected_payload' => [
                'status' => 400,
                'message' => 'Foo bar',
            ],
        ];

        yield 'with_status_code_and_message' => [
            'exception' => new TestRequestException('Test message'),
            'context' => [
                'status_code' => 500,
                'message' => 'Foo bar',
            ],
            'expected_status' => 500,
            'expected_payload' => [
                'status' => 500,
                'message' => 'Foo bar',
            ],
        ];
    }
}
