<?php

declare(strict_types=1);

namespace Tests\Unit\Shared\Ui\Http\ErrorRenderer;

use App\Shared\Ui\Http\ErrorRenderer\ErrorRenderer;
use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\ExceptionMapperInterface;
use App\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\HttpError;
use App\System\Response\ResponseFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Tests\Unit\Shared\Ui\Http\ErrorRenderer\ExceptionMapper\Dummy\TestException;

final class ErrorRendererTest extends TestCase
{
    /**
     * @dataProvider provideExceptions
     */
    public function testItShouldRenderJsonResponseForGivenExceptionAndJsonContentType(
        \Throwable $exception,
        array $exceptionMapping,
        int $expectedStatus,
        array $expectedPayload
    ): void {
        $errorRenderer = new ErrorRenderer($this->createResponseFactory(), false, [], $exceptionMapping);
        $response = $errorRenderer->render($exception, $this->createRequest('application/json'));

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame($expectedStatus, $response->getStatusCode());
        $payload = json_decode($response->getContent(), true);
        $this->assertSame($expectedPayload, $payload);
    }

    /**
     * @dataProvider provideExceptions
     */
    public function testItShouldRenderXmlResponseForGivenExceptionAndXmlContentType(
        \Throwable $exception,
        array $exceptionMapping,
        int $expectedStatus,
        array $expectedPayload
    ): void {
        $errorRenderer = new ErrorRenderer($this->createResponseFactory(), false, [], $exceptionMapping);
        $response = $errorRenderer->render($exception, $this->createRequest('application/xml'));

        $this->assertInstanceOf(Response::class, $response);
        $this->assertSame('application/xml', $response->headers->get('Content-Type'));
        $this->assertSame($expectedStatus, $response->getStatusCode());
        $expectedXml = <<<XML
<?xml version="1.0"?>
<response>
    <error>
        <status>%d</status>
        <message>%s</message>
    </error>
</response>
XML;
        $expectedXml = vsprintf($expectedXml, array_values($expectedPayload['error']));
        $this->assertXmlStringEqualsXmlString($expectedXml, $response->getContent());
    }

    public function provideExceptions(): iterable
    {
        yield 'with_empty_exception_mapping' => [
            'exception' => new \Exception('Test message'),
            'exception_mapping' => [],
            'expected_response_status' => 500,
            'expected_response_payload' => [
                'error' => [
                    'status' => 500,
                    'message' => 'Internal Server Error',
                ],
            ],
        ];

        yield 'with_status_code_mapping' => [
            'exception' => new \InvalidArgumentException('Test message'),
            'exception_mapping' => [
                \InvalidArgumentException::class => [
                    'status_code' => 400,
                ],
            ],
            'expected_response_status' => 400,
            'expected_response_payload' => [
                'error' => [
                    'status' => 400,
                    'message' => 'Test message',
                ],
            ],
        ];

        yield 'with_message_mapping' => [
            'exception' => new \DomainException('Test message'),
            'exception_mapping' => [
                \DomainException::class => [
                    'message' => 'Foo bar',
                ],
            ],
            'expected_response_status' => 500,
            'expected_response_payload' => [
                'error' => [
                    'status' => 500,
                    'message' => 'Foo bar',
                ],
            ],
        ];

        yield 'with_status_code_and_message_mapping' => [
            'exception' => new TestException('Test message'),
            'exception_mapping' => [
                TestException::class => [
                    'status_code' => 400,
                    'message' => 'Foo bar',
                ],
            ],
            'expected_response_status' => 400,
            'expected_response_payload' => [
                'error' => [
                    'status' => 400,
                    'message' => 'Foo bar',
                ],
            ],
        ];
    }

    public function testItShouldDelegateMappingToAppropriateMapper(): void
    {
        $testExceptionMapper = new class() implements ExceptionMapperInterface {
            public function map(\Throwable $exception, array $context = []): ?HttpError
            {
                if (!$exception instanceof TestException) {
                    return null;
                }

                return new HttpError(400, ['title' => 'Bad request', 'detail' => 'Invalid data!']);
            }
        };

        $mappers = [
            $testExceptionMapper,
        ];

        $errorRenderer = new ErrorRenderer($this->createResponseFactory(), false, $mappers);
        $response = $errorRenderer->render(new TestException(), $this->createRequest('application/json'));

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(400, $response->getStatusCode());
        $payload = json_decode($response->getContent(), true);
        $this->assertSame(['error' => ['title' => 'Bad request', 'detail' => 'Invalid data!']], $payload);
    }

    public function testItShouldAppendDebugInfoInDebugMode(): void
    {
        $errorRenderer = new ErrorRenderer($this->createResponseFactory(), true);
        $response = $errorRenderer->render(new TestException(), $this->createRequest('application/json'));

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(500, $response->getStatusCode());
        $payload = json_decode($response->getContent(), true);
        $this->assertCount(2, $payload);
        $this->assertArrayHasKey('error', $payload);
        $this->assertArrayHasKey('debug', $payload);
    }

    private function createResponseFactory(): ResponseFactory
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return new ResponseFactory($serializer);
    }

    private function createRequest(string $contentType): Request
    {
        $request = new Request();
        $request->headers->set('Content-Type', $contentType);

        return $request;
    }
}
