<?php

declare(strict_types=1);

namespace Tests\Unit\System\ValueObject\Date;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Date\Exception\InvalidDateException;
use PHPUnit\Framework\TestCase;

/**
 * Date test
 */
final class DateTest extends TestCase
{
    public function testDateCreatesSuccessfully(): void
    {
        $date = new Date('2022-01-30');
        $this->assertSame('2022-01-30', $date->toString());
    }

    public function testCannotCreateDateWithWrongValue(): void
    {
        $this->expectException(InvalidDateException::class);
        new Date('2021-02-31');
    }

    /**
     * @dataProvider provideEqualDates
     */
    public function testDateEquals(string $date1, string $date2, bool $expectedResult): void
    {
        $date1 = new Date($date1);
        $date2 = new Date($date2);
        $this->assertEquals($expectedResult, $date1->equals($date2));
        $this->assertEquals($expectedResult, $date2->equals($date1));
    }

    public function provideEqualDates(): array
    {
        return [
            ['2021-02-23', '2021-02-23', true],
            ['2021-05-13', '2021-06-13', false],
        ];
    }

    public function testDateGreaterThan(): void
    {
        $date1 = new Date('2022-02-01');
        $date2 = new Date('2022-01-31');
        $date3 = new Date('2022-02-02');
        $this->assertTrue($date1->isGreaterThan($date2));
        $this->assertFalse($date1->isGreaterThan($date3));
        $this->assertFalse($date1->isGreaterThan($date1));
    }

    public function testDateLessThan(): void
    {
        $date1 = new Date('2022-02-01');
        $date2 = new Date('2022-01-31');
        $date3 = new Date('2022-02-02');
        $this->assertTrue($date1->isLessThan($date3));
        $this->assertFalse($date1->isLessThan($date2));
        $this->assertFalse($date1->isLessThan($date1));
    }
}
