<?php

declare(strict_types=1);

namespace Tests\Unit\System\ValueObject\Uuid;

use App\System\ValueObject\Uuid\Exception\InvalidUuidException;
use App\System\ValueObject\Uuid\Uuid;
use PHPUnit\Framework\TestCase;

final class UuidTest extends TestCase
{
    public function testCreateUuid(): void
    {
        $expectedUuid = '3210bc29-d721-46eb-97be-7fa851b29ec5';
        $uuid = Uuid::fromString($expectedUuid);
        $this->assertSame($expectedUuid, (string) $uuid);
        $this->assertSame($expectedUuid, $uuid->toString());
    }

    public function testCannotCreateUuidWithWrongValue(): void
    {
        $this->expectException(InvalidUuidException::class);
        Uuid::fromString('invalid');
    }

    public function testCreateUuidFromString(): void
    {
        $this->assertInstanceOf(Uuid::class, Uuid::fromString('3210bc29-d721-46eb-97be-7fa851b29ec5'));
    }

    public function testCreateRandomUuid(): void
    {
        $this->assertInstanceOf(Uuid::class, Uuid::random());
    }

    /**
     * @dataProvider provideEqualUuids
     */
    public function testUuidEquals(string $uuid1, string $uuid2, bool $equals): void
    {
        $uuid1 = Uuid::fromString($uuid1);
        $uuid2 = Uuid::fromString($uuid2);
        $this->assertSame($equals, $uuid1->equals($uuid2));
        $this->assertSame($equals, $uuid2->equals($uuid1));
    }

    public function provideEqualUuids(): array
    {
        return [
            ['3210bc29-d721-46eb-97be-7fa851b29ec5', '3210bc29-d721-46eb-97be-7fa851b29ec5', true],
            ['3210bc29-d721-46eb-97be-7fa851b29ec5', '6780f600-fa78-431d-b6b6-21236cf94e37', false],
        ];
    }
}
