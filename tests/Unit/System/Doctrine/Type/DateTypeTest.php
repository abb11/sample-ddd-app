<?php

declare(strict_types=1);

namespace Tests\Unit\System\Doctrine\Type;

use App\System\Doctrine\Type\DateType;
use App\System\ValueObject\Date\Date;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use PHPUnit\Framework\TestCase;

final class DateTypeTest extends TestCase
{
    private DateType $type;

    private AbstractPlatform $platform;

    public static function setUpBeforeClass(): void
    {
        Type::addType(DateType::NAME . '_test', DateType::class);
    }

    protected function setUp(): void
    {
        $this->type = Type::getType(DateType::NAME . '_test');
        $this->platform = \Mockery::mock(AbstractPlatform::class)->makePartial();
    }

    public function testNullToPHPValue(): void
    {
        $actual = $this->type->convertToPHPValue(null, $this->platform);
        $this->assertNull($actual);
    }

    /**
     * @dataProvider provideValidDatabaseToPHPValues
     */
    public function testValidDatabaseToPHPValue(mixed $input, Date $date): void
    {
        $actual = $this->type->convertToPHPValue($input, $this->platform);
        $this->assertEquals($date, $actual);
    }

    public function provideValidDatabaseToPHPValues(): array
    {
        $str = '2023-02-04';
        $date = Date::fromString($str);

        return [
            [$date, $date],
            [$str, $date],
        ];
    }

    /**
     * @dataProvider provideInvalidDatabaseToPHPValues
     */
    public function testInvalidDatabaseToPHPValue(mixed $input): void
    {
        $this->expectException(ConversionException::class);
        $this->type->convertToPHPValue($input, $this->platform);
    }

    public function provideInvalidDatabaseToPHPValues(): array
    {
        return [
            'int' => [123],
            'float' => [123.55],
            'string' => ['abcdef'],
            'wrong date format' => ['04-02-2023'],
        ];
    }

    public function testNullToDatabaseValue(): void
    {
        $actual = $this->type->convertToDatabaseValue(null, $this->platform);
        $this->assertNull($actual);
    }

    public function testEmptyStringToDatabaseValue(): void
    {
        $actual = $this->type->convertToDatabaseValue('', $this->platform);
        $this->assertNull($actual);
    }

    /**
     * @dataProvider provideValidPHPToDatabaseValues
     */
    public function testValidPHPToDatabaseValue(mixed $input, string $date): void
    {
        $actual = $this->type->convertToDatabaseValue($input, $this->platform);
        $this->assertSame($date, $actual);
    }

    public function provideValidPHPToDatabaseValues(): array
    {
        $str = '2023-02-04';
        $date = Date::fromString($str);

        return [
            [$date, $str],
        ];
    }

    /**
     * @dataProvider provideInvalidPHPToDatabaseValues
     */
    public function testInvalidPHPToDatabaseValue(mixed $input): void
    {
        $this->expectException(ConversionException::class);
        $this->type->convertToDatabaseValue($input, $this->platform);
    }

    public function provideInvalidPHPToDatabaseValues(): array
    {
        return [
            [12345],
            [12.55],
            ['abcdef'],
        ];
    }
}
