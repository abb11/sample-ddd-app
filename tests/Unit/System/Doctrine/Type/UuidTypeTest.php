<?php

declare(strict_types=1);

namespace Tests\Unit\System\Doctrine\Type;

use App\System\Doctrine\Type\UuidType;
use App\System\ValueObject\Uuid\Uuid;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use PHPUnit\Framework\TestCase;

final class UuidTypeTest extends TestCase
{
    private UuidType $type;

    private AbstractPlatform $platform;

    public static function setUpBeforeClass(): void
    {
        Type::addType(UuidType::NAME . '_test', UuidType::class);
    }

    protected function setUp(): void
    {
        $this->type = Type::getType(UuidType::NAME . '_test');
        $this->platform = \Mockery::mock(AbstractPlatform::class)->makePartial();
    }

    public function testNullToPHPValue(): void
    {
        $actual = $this->type->convertToPHPValue(null, $this->platform);
        $this->assertNull($actual);
    }

    /**
     * @dataProvider provideValidDatabaseToPHPValues
     */
    public function testValidDatabaseToPHPValue(mixed $input, Uuid $uuid): void
    {
        $actual = $this->type->convertToPHPValue($input, $this->platform);
        $this->assertEquals($uuid, $actual);
    }

    public function provideValidDatabaseToPHPValues(): array
    {
        $str = 'e8579cd2-542a-4d20-912c-e56cb93ee5ad';
        $uuid = Uuid::fromString($str);

        return [
            [$uuid, $uuid],
            [$str, $uuid],
        ];
    }

    /**
     * @dataProvider provideInvalidDatabaseToPHPValues
     */
    public function testInvalidDatabaseToPHPValue(mixed $input): void
    {
        $this->expectException(ConversionException::class);
        $this->type->convertToPHPValue($input, $this->platform);
    }

    public function provideInvalidDatabaseToPHPValues(): array
    {
        return [
            [12345],
            [12.55],
            ['abcdef'],
        ];
    }

    public function testNullToDatabaseValue(): void
    {
        $actual = $this->type->convertToDatabaseValue(null, $this->platform);
        $this->assertNull($actual);
    }

    public function testEmptyStringToDatabaseValue(): void
    {
        $actual = $this->type->convertToDatabaseValue('', $this->platform);
        $this->assertNull($actual);
    }

    /**
     * @dataProvider provideValidPHPToDatabaseValues
     */
    public function testValidPHPToDatabaseValue(mixed $input, string $uuid): void
    {
        $actual = $this->type->convertToDatabaseValue($input, $this->platform);
        $this->assertSame($uuid, $actual);
    }

    public function provideValidPHPToDatabaseValues(): array
    {
        $str = 'e8579cd2-542a-4d20-912c-e56cb93ee5ad';
        $uuid = Uuid::fromString($str);

        return [
            [$str, $str],
            [$uuid, $str],
        ];
    }

    /**
     * @dataProvider provideInvalidPHPToDatabaseValues
     */
    public function testInvalidPHPToDatabaseValue(mixed $input): void
    {
        $this->expectException(ConversionException::class);
        $this->type->convertToDatabaseValue($input, $this->platform);
    }

    public function provideInvalidPHPToDatabaseValues(): array
    {
        return [
            [123456],
            [12.55],
            ['abcdef'],
        ];
    }
}
