<?php

declare(strict_types=1);

namespace Tests\Unit\System\Utils;

use App\System\Utils\ArrayAccessor;
use PHPUnit\Framework\TestCase;

/**
 * ArrayAccessorTest
 */
final class ArrayAccessorTest extends TestCase
{
    public function testGetValue(): void
    {
        $array = [
            'id' => 1,
            'title' => 'Test',
            'details' => [
                'status' => 'NEW',
            ],
            'tags' => [
                'tag_1',
                'tag_2',
            ],
        ];
        $arrayAccessor = new ArrayAccessor($array);

        $this->assertSame(1, $arrayAccessor->get('id'));
        $this->assertSame('Test', $arrayAccessor->get('title'));
        $this->assertSame('NEW', $arrayAccessor->get('details.status'));
        $this->assertSame('tag_1', $arrayAccessor->get('tags.0'));
        $this->assertSame('tag_2', $arrayAccessor->get('tags.1'));
    }

    public function testGetDefaultValue(): void
    {
        $arrayAccessor = new ArrayAccessor([
            'title' => 'Test',
        ]);

        $this->assertNull($arrayAccessor->get('name'));
        $this->assertNull($arrayAccessor->get('details.status'));
        $this->assertSame('default', $arrayAccessor->get('name', 'default'));
    }

    public function testGetArray(): void
    {
        $array = [
            'title' => 'Test',
        ];
        $arrayAccessor = new ArrayAccessor($array);

        $this->assertSame($array, $arrayAccessor->getArray());
    }

    public function testHasValue(): void
    {
        $arrayAccessor = new ArrayAccessor([
            'title' => 'Test',
            'tags' => null,
            'details' => [
                'status' => 'NEW',
            ],
        ]);

        $this->assertTrue($arrayAccessor->has('title'));
        $this->assertTrue($arrayAccessor->has('tags'));
        $this->assertTrue($arrayAccessor->has('details.status'));
        $this->assertFalse($arrayAccessor->has('id'));
        $this->assertFalse($arrayAccessor->has('details.priority'));
    }

    /**
     * @depends testGetArray
     */
    public function testSetValue(): void
    {
        $arrayAccessor = new ArrayAccessor([
            'id' => 1,
            'title' => 'Test',
            'details' => [
                'status' => 'NEW',
            ],
        ]);

        $arrayAccessor->set('title', 'New test');
        $arrayAccessor->set('details.status', 'CLOSED');
        $arrayAccessor->set('details.priority', 1);
        $arrayAccessor->set('tags.1', 'tag_1');
        $arrayAccessor->set('tags.2', 'tag_2');

        $expectedArray = [
            'id' => 1,
            'title' => 'New test',
            'details' => [
                'status' => 'CLOSED',
                'priority' => 1,
            ],
            'tags' => [
                1 => 'tag_1',
                2 => 'tag_2',
            ],
        ];

        $this->assertSame($expectedArray, $arrayAccessor->getArray());
    }
}
