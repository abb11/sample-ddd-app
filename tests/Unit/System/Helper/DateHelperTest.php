<?php

declare(strict_types=1);

namespace Tests\Unit\System\Helper;

use App\System\Helper\DateHelper;
use PHPUnit\Framework\TestCase;

final class DateHelperTest extends TestCase
{
    public function testIsValidDateInFormat(): void
    {
        $this->assertTrue(DateHelper::isValidDateInFormat('2021-11-02 16:48', 'Y-m-d H:i'));
        $this->assertFalse(DateHelper::isValidDateInFormat('2021-11-02 16:48', 'Y-m-d H:i:s'));
    }
}
