<?php

declare(strict_types=1);

namespace Tests\Unit\System\Helper;

use App\System\Helper\ExceptionHelper;
use PHPUnit\Framework\TestCase;

final class ExceptionHelperTest extends TestCase
{
    public function testItShouldConvertExceptionToArray(): void
    {
        $prev = new \InvalidArgumentException('Previous', 2);
        $exception = new \RuntimeException('Test', 1, $prev);

        $expectedArray = [
            'exception' => 'RuntimeException',
            'message' => 'Test',
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'code' => 1,
            'trace' => $exception->getTraceAsString(),
        ];

        $this->assertSame($expectedArray, ExceptionHelper::convertToArray($exception));

        $expectedArray['previous'] = [
            'exception' => 'InvalidArgumentException',
            'message' => 'Previous',
            'file' => $prev->getFile(),
            'line' => $prev->getLine(),
            'code' => 2,
            'trace' => $prev->getTraceAsString(),
        ];

        $this->assertSame($expectedArray, ExceptionHelper::convertToArray($exception, true));

        $exception = new \Exception('Without previous');

        $expectedArray = [
            'exception' => 'Exception',
            'message' => 'Without previous',
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'code' => 0,
            'trace' => $exception->getTraceAsString(),
        ];

        $this->assertSame($expectedArray, ExceptionHelper::convertToArray($exception, true));
    }
}
