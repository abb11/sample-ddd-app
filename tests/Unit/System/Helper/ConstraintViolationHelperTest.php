<?php

declare(strict_types=1);

namespace Tests\Unit\System\Helper;

use App\System\Helper\ConstraintViolationHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

final class ConstraintViolationHelperTest extends TestCase
{
    public function testItShouldConvertViolationsToArray(): void
    {
        $violations = new ConstraintViolationList();
        $violations->add(new ConstraintViolation('This value should not be blank.', '', [], null, '[person][name]', null));
        $violations->add(new ConstraintViolation('This value should not be blank.', '', [], null, '[date]', null));
        $violations->add(new ConstraintViolation('This value is not a valid date.', '', [], null, '[date]', null));
        $violations->add(new ConstraintViolation('This value should not be blank.', '', [], null, 'comments[0].text', null));

        $expectedArray = [
            'person.name' => [
                'This value should not be blank.',
            ],
            'date' => [
                'This value should not be blank.',
                'This value is not a valid date.',
            ],
            'comments.0.text' => [
                'This value should not be blank.',
            ],
        ];

        $this->assertSame($expectedArray, ConstraintViolationHelper::convertToArray($violations));
    }
}
