<?php

declare(strict_types=1);

namespace Tests\Unit\Task\Domain\Task;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Domain\Task\Priority\Priority;
use App\Task\Domain\Task\Task;
use App\Task\Domain\Task\TaskOutsideInterface;
use Tests\Utils\DomainEventCollector;

final class TaskBuilder
{
    private TaskOutsideInterface $taskOutside;

    private Uuid $id;

    private string $title = 'Task title';

    private string $priority = Priority::NORMAL;

    private ?Date $executionDay = null;

    private bool $shouldBeClosed = false;

    private bool $shouldBeCanceled = false;

    private function __construct()
    {
        $this->taskOutside = TaskOutsideBuilder::aTaskOutside()->build();
        $this->id = Uuid::random();
    }

    public static function aTask(): self
    {
        return new self();
    }

    public function withId(string $uuid): self
    {
        $this->id = Uuid::fromString($uuid);

        return $this;
    }

    public function withTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function withPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function withExecutionDay(?Date $executionDay): self
    {
        $this->executionDay = $executionDay;

        return $this;
    }

    public function shouldBeClosed(): self
    {
        $this->shouldBeClosed = true;

        return $this;
    }

    public function shouldBeCanceled(): self
    {
        $this->shouldBeCanceled = true;

        return $this;
    }

    public function build(): Task
    {
        $task = new Task(
            $this->taskOutside,
            $this->id,
            $this->title,
            $this->priority,
            $this->executionDay
        );

        if ($this->shouldBeClosed) {
            $task->close();
        }

        if ($this->shouldBeCanceled) {
            $task->cancel();
        }

        DomainEventCollector::instance()->clear();

        return $task;
    }
}
