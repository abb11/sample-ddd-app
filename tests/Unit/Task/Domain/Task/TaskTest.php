<?php

declare(strict_types=1);

namespace Tests\Unit\Task\Domain\Task;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Domain\Task\Event\TaskCanceledEvent;
use App\Task\Domain\Task\Event\TaskClosedEvent;
use App\Task\Domain\Task\Event\TaskCreatedEvent;
use App\Task\Domain\Task\Event\TaskOpenedEvent;
use App\Task\Domain\Task\Event\TaskUpdatedEvent;
use App\Task\Domain\Task\Exception\UnableToCancelClosedTaskException;
use App\Task\Domain\Task\Exception\UnableToCloseCanceledTaskException;
use App\Task\Domain\Task\Exception\UnableToOpenCanceledTaskException;
use App\Task\Domain\Task\Priority\Priority;
use App\Task\Domain\Task\Status\Status;
use App\Task\Domain\Task\Task;
use Tests\Unit\Shared\Domain\AggregateTestCase;

final class TaskTest extends AggregateTestCase
{
    public function testTaskCreatesSuccessfully(): void
    {
        new Task(
            TaskOutsideBuilder::aTaskOutside()->build(),
            Uuid::fromString('3210bc29-d721-46eb-97be-7fa851b29ec5'),
            'Task create test',
            Priority::LOW,
            null
        );

        $events = $this->getDomainEvents();

        $this->assertCount(1, $events, 'Only one event should be in the buffer');
        $taskCreatedEvent = $events[0];
        $this->assertInstanceOf(TaskCreatedEvent::class, $taskCreatedEvent);
        $this->assertSame('3210bc29-d721-46eb-97be-7fa851b29ec5', $taskCreatedEvent->taskId->toString());
        $this->assertSame('Task create test', $taskCreatedEvent->title);
        $this->assertSame(Status::OPEN, $taskCreatedEvent->status);
        $this->assertSame(Priority::LOW, $taskCreatedEvent->priority);
        $this->assertInstanceOf(\DateTimeImmutable::class, $taskCreatedEvent->createdAt);
        $this->assertNull($taskCreatedEvent->executionDay);
    }

    public function testTaskParamsChangeSuccessfully(): void
    {
        $task = TaskBuilder::aTask()
            ->withId('3210bc29-d721-46eb-97be-7fa851b29ec5')
            ->withTitle('Task title')
            ->withPriority(Priority::NORMAL)
            ->withExecutionDay(null)
            ->build();

        $today = (new \DateTimeImmutable())->format('Y-m-d');
        $task->update('New title', Priority::HIGH, new Date($today));

        $events = $this->getDomainEvents();
        $taskUpdatedEvent = $events[0];
        $this->assertInstanceOf(TaskUpdatedEvent::class, $taskUpdatedEvent);
        $this->assertSame('3210bc29-d721-46eb-97be-7fa851b29ec5', $taskUpdatedEvent->taskId->toString());
        $this->assertSame('New title', $taskUpdatedEvent->newTitle);
        $this->assertSame(Priority::HIGH, $taskUpdatedEvent->newPriority);
        $this->assertSame($today, $taskUpdatedEvent->newExecutionDay->toString());
    }

    public function testTaskClosesSuccessfully(): void
    {
        $task = TaskBuilder::aTask()
            ->withId('3210bc29-d721-46eb-97be-7fa851b29ec5')
            ->build();

        $task->close();

        $events = $this->getDomainEvents();
        $this->assertCount(1, $events);
        $taskClosedEvent = $events[0];
        $this->assertInstanceOf(TaskClosedEvent::class, $taskClosedEvent);
        $this->assertSame('3210bc29-d721-46eb-97be-7fa851b29ec5', $taskClosedEvent->taskId->toString());
    }

    public function testTaskCancelsSuccessfully(): void
    {
        $task = TaskBuilder::aTask()
            ->withId('3210bc29-d721-46eb-97be-7fa851b29ec5')
            ->build();

        $task->cancel();

        $events = $this->getDomainEvents();
        $this->assertCount(1, $events);
        $taskCanceledEvent = $events[0];
        $this->assertInstanceOf(TaskCanceledEvent::class, $taskCanceledEvent);
        $this->assertSame('3210bc29-d721-46eb-97be-7fa851b29ec5', $taskCanceledEvent->taskId->toString());
    }

    public function testClosedTaskOpensSuccessfully(): void
    {
        $task = TaskBuilder::aTask()
            ->withId('3210bc29-d721-46eb-97be-7fa851b29ec5')
            ->shouldBeClosed()
            ->build();

        $task->open();

        $events = $this->getDomainEvents();
        $this->assertCount(1, $events);
        $taskCanceledEvent = $events[0];
        $this->assertInstanceOf(TaskOpenedEvent::class, $taskCanceledEvent);
        $this->assertSame('3210bc29-d721-46eb-97be-7fa851b29ec5', $taskCanceledEvent->taskId->toString());
    }

    public function testCannotCloseCanceledTask(): void
    {
        $task = TaskBuilder::aTask()->shouldBeCanceled()->build();
        $this->expectException(UnableToCloseCanceledTaskException::class);
        $task->close();
    }

    public function testCannotOpenCanceledTask(): void
    {
        $task = TaskBuilder::aTask()->shouldBeCanceled()->build();
        $this->expectException(UnableToOpenCanceledTaskException::class);
        $task->open();
    }

    public function testCannotCancelClosedTask(): void
    {
        $task = TaskBuilder::aTask()->shouldBeClosed()->build();
        $this->expectException(UnableToCancelClosedTaskException::class);
        $task->cancel();
    }
}
