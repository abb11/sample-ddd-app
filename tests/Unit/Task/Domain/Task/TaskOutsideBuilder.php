<?php

declare(strict_types=1);

namespace Tests\Unit\Task\Domain\Task;

use App\Task\Domain\Task\TaskOutsideInterface;
use Tests\Utils\DomainEventCollector;

class TaskOutsideBuilder
{
    private \DateTimeImmutable $currentTime;

    private function __construct()
    {
        $this->currentTime = new \DateTimeImmutable();
    }

    public static function aTaskOutside(): self
    {
        return new self();
    }

    public function withCurrentTime(\DateTimeImmutable $currentTime): self
    {
        $this->currentTime = $currentTime;

        return $this;
    }

    public function build(): TaskOutsideInterface
    {
        $mock = \Mockery::mock(TaskOutsideInterface::class);
        $mock->allows('getCurrentTime')->andReturn($this->currentTime);
        $mock->allows('publishEvent')->andReturnUsing(fn (object $event) => DomainEventCollector::instance()->add($event));

        return $mock;
    }
}
