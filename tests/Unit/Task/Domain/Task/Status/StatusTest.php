<?php

declare(strict_types=1);

namespace Tests\Unit\Task\Domain\Task\Status;

use App\Task\Domain\Task\Status\Exception\InvalidStatusException;
use App\Task\Domain\Task\Status\Status;
use PHPUnit\Framework\TestCase;

/**
 * Status test
 */
final class StatusTest extends TestCase
{
    /**
     * @dataProvider provideValidStatuses
     */
    public function testStatusCreatesSuccessfully(string $status): void
    {
        $this->expectNotToPerformAssertions();
        new Status($status);
    }

    public function provideValidStatuses(): array
    {
        return [
            [Status::OPEN],
            [Status::CLOSED],
            [Status::CANCELED],
        ];
    }

    public function testCannotCreateWrongStatus(): void
    {
        $this->expectException(InvalidStatusException::class);
        new Status('wrong_status_value');
    }

    /**
     * @dataProvider provideStatusChanges
     */
    public function testStatusChangesSuccessfully(string $status, string $newStatus): void
    {
        $statusObj = new Status($status);
        $newStatusObj = $statusObj->change($newStatus);

        $this->assertInstanceOf(Status::class, $newStatusObj);
        $this->assertNotSame($statusObj, $newStatusObj);
        $this->assertSame($newStatus, $newStatusObj->getValue());
        $this->assertTrue($newStatusObj->is($newStatus));
    }

    public function provideStatusChanges(): array
    {
        return [
            [Status::OPEN, Status::CLOSED],
            [Status::OPEN, Status::CANCELED],
            [Status::CLOSED, Status::OPEN],
            [Status::CLOSED, Status::CANCELED],
            [Status::CANCELED, Status::OPEN],
            [Status::CANCELED, Status::CLOSED],
        ];
    }

    /**
     * @dataProvider provideValidStatuses
     */
    public function testStatusDoesNotChangeIfTheSameValueIsGiven(string $status): void
    {
        $statusObj = new Status($status);
        $this->assertSame($statusObj, $statusObj->change($status));
    }
}
