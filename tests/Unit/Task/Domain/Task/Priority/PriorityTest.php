<?php

declare(strict_types=1);

namespace Tests\Unit\Task\Domain\Task\Priority;

use App\Task\Domain\Task\Priority\Exception\InvalidPriorityException;
use App\Task\Domain\Task\Priority\Priority;
use PHPUnit\Framework\TestCase;

/**
 * Priority test
 */
final class PriorityTest extends TestCase
{
    /**
     * @dataProvider provideValidPriorities
     */
    public function testPriorityCreatesSuccessfully(string $priority): void
    {
        $this->expectNotToPerformAssertions();
        new Priority($priority);
    }

    public function provideValidPriorities(): array
    {
        return [
            [Priority::LOW],
            [Priority::NORMAL],
            [Priority::HIGH],
        ];
    }

    public function testCannotCreateWrongPriority(): void
    {
        $this->expectException(InvalidPriorityException::class);
        new Priority('wrong_priority_value');
    }

    /**
     * @dataProvider providePriorityChanges
     */
    public function testPriorityChangesSuccessfully(string $priority, string $newPriority): void
    {
        $priorityObj = new Priority($priority);
        $newPriorityObj = $priorityObj->change($newPriority);

        $this->assertInstanceOf(Priority::class, $newPriorityObj);
        $this->assertNotSame($priorityObj, $newPriorityObj);
        $this->assertSame($newPriority, $newPriorityObj->getValue());
    }

    public function providePriorityChanges(): array
    {
        return [
            [Priority::LOW, Priority::NORMAL],
            [Priority::LOW, Priority::HIGH],
            [Priority::NORMAL, Priority::LOW],
            [Priority::NORMAL, Priority::HIGH],
            [Priority::HIGH, Priority::LOW],
            [Priority::HIGH, Priority::NORMAL],
        ];
    }

    /**
     * @dataProvider provideValidPriorities
     */
    public function testPriorityDoesNotChangeIfTheSameValueIsGiven(string $priority): void
    {
        $priorityObj = new Priority($priority);
        $this->assertSame($priorityObj, $priorityObj->change($priority));
    }
}
