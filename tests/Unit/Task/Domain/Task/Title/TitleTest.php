<?php

declare(strict_types=1);

namespace Tests\Unit\Task\Domain\Task\Title;

use App\Task\Domain\Task\Title\Exception\WrongTitleLengthException;
use App\Task\Domain\Task\Title\Title;
use PHPUnit\Framework\TestCase;

/**
 * Title test
 */
final class TitleTest extends TestCase
{
    /**
     * @dataProvider providerValidTitles
     */
    public function testTitleCreatesSuccessfully(string $title): void
    {
        $this->expectNotToPerformAssertions();
        new Title($title);
    }

    public function providerValidTitles(): array
    {
        return [
            [str_repeat('X', Title::MIN_LENGTH)],
            [str_repeat('X', Title::MAX_LENGTH)],
        ];
    }

    /**
     * @dataProvider providerTitlesWithWrongLength
     */
    public function testCannotCreateTitleWithWrongLength(string $title): void
    {
        $this->expectException(WrongTitleLengthException::class);
        new Title($title);
    }

    public function providerTitlesWithWrongLength(): array
    {
        return [
            [str_repeat('X', Title::MIN_LENGTH - 1)],
            [str_repeat('X', Title::MAX_LENGTH + 1)],
        ];
    }

    public function testTitleChangesSuccessfully(): void
    {
        $title = new Title('title');
        $newTitle = $title->change('new title');
        $this->assertInstanceOf(Title::class, $newTitle);
        $this->assertNotSame($title, $newTitle);
        $this->assertSame('new title', $newTitle->getValue());
    }

    public function testTitleDoesNotChangeIfTheSameValueIsGiven(): void
    {
        $title = new Title('title');
        $newTitle = $title->change('title');
        $this->assertSame($title, $newTitle);
    }
}
