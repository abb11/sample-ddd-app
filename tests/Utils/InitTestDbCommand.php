<?php

declare(strict_types=1);

namespace Tests\Utils;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:tests:init-db',
    description: 'Initialize database for tests'
)]
final class InitTestDbCommand extends Command
{
    private SymfonyStyle $io;

    public function __construct(
        private readonly Connection $connection,
    ) {
        parent::__construct();
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->runCommand('doctrine:database:drop', [
            '--if-exists' => 1,
            '--force' => 1,
        ]);

        $this->runCommand('doctrine:database:create');

        $this->runCommand('doctrine:migrations:migrate');

        $this->io->success(sprintf(
            'Database "%s" was successfully initialized',
            $this->connection->getParams()['dbname']
        ));

        return 0;
    }

    private function runCommand(string $commandName, array $arguments = []): int
    {
        $command = $this->getApplication()->find($commandName);
        $input = new ArrayInput($arguments);
        $input->setInteractive(false);

        return $command->run($input, $this->io);
    }
}
