<?php

declare(strict_types=1);

namespace Tests\Utils\Fixture;

use App\Shared\Application\Bus\CommandBusInterface;

class FixtureFactory
{
    public function __construct(
        private readonly CommandBusInterface $commandBus,
    ) {
    }

    /**
     * @template T
     *
     * @param class-string<T> $fixtureClass
     *
     * @return T
     */
    public function create(string $fixtureClass): mixed
    {
        if (!is_subclass_of($fixtureClass, AbstractFixture::class)) {
            throw new \InvalidArgumentException(sprintf(
                'Fixture "%s" must extend "%s".',
                $fixtureClass,
                AbstractFixture::class
            ));
        }

        return new $fixtureClass($this->commandBus);
    }
}
