<?php

declare(strict_types=1);

namespace Tests\Utils\Fixture;

use App\Shared\Application\Bus\CommandBusInterface;

abstract class AbstractFixture
{
    private bool $fixtureBuilt = false;

    final public function __construct(
        protected readonly CommandBusInterface $commandBus,
    ) {
        $this->init();
    }

    protected function init(): void
    {
    }

    public function build(): void
    {
        if ($this->fixtureBuilt) {
            throw new \RuntimeException(sprintf('Fixture "%s" is already built.', static::class));
        }

        $this->doBuild();
        $this->fixtureBuilt = true;
    }

    abstract protected function doBuild(): void;
}
