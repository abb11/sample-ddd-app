<?php

declare(strict_types=1);

namespace Tests\Utils;

final class DomainEventCollector
{
    private static self $instance;

    private array $events = [];

    private function __construct()
    {
    }

    public static function instance(): self
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function add(object $event): void
    {
        $this->events[] = $event;
    }

    public function clear(): void
    {
        $this->events = [];
    }

    public function get(): array
    {
        return $this->events;
    }
}
