<?php

declare(strict_types=1);

namespace Tests\Utils\Assertion;

use PHPUnit\Framework\TestCase;

final class DateAssertion
{
    /**
     * Asserts that a date has an expected format
     *
     * @param mixed  $date           String date
     * @param string $expectedFormat Expected format
     */
    public static function assertDateFormat(
        mixed $date,
        string $expectedFormat = \DateTimeInterface::ATOM
    ): void {
        TestCase::assertIsString($date, 'Date should be of type "string".');

        $isValid = ($d = \DateTime::createFromFormat($expectedFormat, $date))
            && $d->format($expectedFormat) === $date;

        if (!$isValid) {
            TestCase::fail(sprintf('Date "%s" has an invalid format. Expected format is "%s".', $date, $expectedFormat));
        }
    }
}
