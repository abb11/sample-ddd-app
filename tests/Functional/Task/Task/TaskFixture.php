<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Command\Cancel\CancelTaskCommand;
use App\Task\Application\Task\Command\Close\CloseTaskCommand;
use App\Task\Application\Task\Command\Create\CreateTaskCommand;
use App\Task\Domain\Task\Priority\Priority;
use Tests\Utils\Fixture\AbstractFixture;

final class TaskFixture extends AbstractFixture
{
    private Uuid $id;

    private string $title;

    private string $priority;

    private ?Date $executionDay = null;

    private bool $shouldBeClosed = false;

    private bool $shouldBeCanceled = false;

    protected function init(): void
    {
        $this->id = Uuid::random();
        $this->title = uniqid('Task_');
        $this->priority = Priority::NORMAL;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function setExecutionDay(?Date $executionDay): self
    {
        $this->executionDay = $executionDay;

        return $this;
    }

    public function shouldBeClosed(): self
    {
        $this->shouldBeClosed = true;

        return $this;
    }

    public function shouldBeCanceled(): self
    {
        $this->shouldBeCanceled = true;

        return $this;
    }

    protected function doBuild(): void
    {
        $this->commandBus->executeCommand(new CreateTaskCommand(
            $this->id,
            $this->title,
            $this->priority,
            $this->executionDay,
        ));

        if ($this->shouldBeClosed) {
            $this->commandBus->executeCommand(new CloseTaskCommand($this->id));
        }

        if ($this->shouldBeCanceled) {
            $this->commandBus->executeCommand(new CancelTaskCommand($this->id));
        }
    }
}
