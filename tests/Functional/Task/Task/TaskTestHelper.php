<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task;

use App\Shared\Application\Bus\QueryBusInterface;
use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Projection\Dto\TaskDto;
use App\Task\Application\Task\Query\Get\GetTaskQuery;

class TaskTestHelper
{
    public function __construct(
        private readonly QueryBusInterface $queryBus,
    ) {
    }

    public function getTask(Uuid|string $id): TaskDto
    {
        if (is_string($id)) {
            $id = Uuid::fromString($id);
        }

        return $this->queryBus->executeQuery(new GetTaskQuery($id))->task;
    }
}
