<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task;

use App\System\ValueObject\Uuid\Uuid;
use Tests\Utils\Fixture\FixtureFactory;

class TaskStory
{
    public function __construct(
        private readonly FixtureFactory $fixtureFactory,
    ) {
    }

    public function givenTaskWithDataExists(array $data): void
    {
        $fixture = $this->fixtureFactory->create(TaskFixture::class);

        if (isset($data['id'])) {
            $fixture->setId($data['id']);
        }

        if (isset($data['title'])) {
            $fixture->setTitle($data['title']);
        }

        if (isset($data['priority'])) {
            $fixture->setPriority($data['priority']);
        }

        if (isset($data['executionDay'])) {
            $fixture->setExecutionDay($data['executionDay']);
        }

        $fixture->build();
    }

    public function givenOpenTaskWithIdExists(Uuid $id): void
    {
        $this->fixtureFactory->create(TaskFixture::class)->setId($id)->build();
    }

    public function givenClosedTaskWithIdExists(Uuid $id): void
    {
        $this->fixtureFactory->create(TaskFixture::class)->setId($id)->shouldBeClosed()->build();
    }

    public function givenCanceledTaskWithIdExists(Uuid $id): void
    {
        $this->fixtureFactory->create(TaskFixture::class)->setId($id)->shouldBeCanceled()->build();
    }

    public function givenManyTasksExist(int $count): void
    {
        for ($i = 0; $i < $count; ++$i) {
            $this->fixtureFactory->create(TaskFixture::class)->build();
        }
    }
}
