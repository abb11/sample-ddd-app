<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task\Action;

use App\System\ValueObject\Uuid\Uuid;
use Tests\Functional\Shared\Action\ActionTestCase;
use Tests\Functional\Shared\Action\ErrorResponseAssertion;
use Tests\Functional\Task\Task\TaskStory;

final class CloseTaskActionTest extends ActionTestCase
{
    public function testTaskClosesSuccessfully(): void
    {
        $client = static::createClient();

        $taskId = Uuid::fromString('21122006-46aa-411a-bebc-367436e8fede');

        $this->getService(TaskStory::class)->givenOpenTaskWithIdExists($taskId);

        $client->request('PATCH', '/api/v1/tasks/' . $taskId->toString() . '/close');

        $this->assertResponseStatusCodeSame(204);
        $this->assertEmpty($client->getResponse()->getContent());
    }

    public function testCannotCloseCanceledTask(): void
    {
        $client = static::createClient();

        $taskId = Uuid::fromString('21122006-46aa-411a-bebc-367436e8fede');

        $this->getService(TaskStory::class)->givenCanceledTaskWithIdExists($taskId);

        $client->request('PATCH', '/api/v1/tasks/' . $taskId->toString() . '/close');

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(422)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 422,
                    'message' => 'Unable to close canceled task',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }

    public function testNotFoundErrorResponseShouldBeReturnedWhenRequestedTaskDoesNotExist(): void
    {
        $nonexistentTaskId = '6fe37f5a-66e2-48ff-b57d-568f9262c755';
        $client = static::createClient();
        $client->request('PATCH', '/api/v1/tasks/' . $nonexistentTaskId . '/close');

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(404)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 404,
                    'message' => 'Task with ID "6fe37f5a-66e2-48ff-b57d-568f9262c755" does not exist',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }
}
