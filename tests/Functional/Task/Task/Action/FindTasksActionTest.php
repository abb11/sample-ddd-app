<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task\Action;

use App\System\ValueObject\Date\Date;
use Tests\Functional\Shared\Action\ActionTestCase;
use Tests\Functional\Shared\Action\ErrorResponseAssertion;
use Tests\Functional\Task\Task\TaskStory;

final class FindTasksActionTest extends ActionTestCase
{
    public function testTasksWithoutFilteringFetchSuccessfully(): void
    {
        $client = static::createClient();

        $this->getService(TaskStory::class)->givenManyTasksExist(4);

        $client->request('GET', '/api/v1/tasks');

        $this->assertResponseStatusCodeSame(200);
        $this->assertJson($client->getResponse()->getContent());

        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('pageCount', $result);
        $this->assertSame(2, $result['pageCount'], 'Invalid page count.');
        $this->assertArrayHasKey('totalTaskCount', $result);
        $this->assertSame(4, $result['totalTaskCount'], 'Invalid total task count.');
        $this->assertArrayHasKey('tasks', $result);
        $this->assertIsArray($result['tasks']);
        $this->assertCount(3, $result['tasks'], 'Invalid number of tasks on page.');

        foreach ($result['tasks'] as $task) {
            TaskAssertion::assertTaskStructure($task);
        }
    }

    public function testTasksWithFilteringFetchSuccessfully(): void
    {
        $client = static::createClient();

        for ($i = 0; $i < 2; ++$i) {
            $this->getService(TaskStory::class)
                ->givenTaskWithDataExists(['executionDay' => Date::fromString('2021-03-27')]);
        }

        $client->request('GET', '/api/v1/tasks?executionDay=2021-03-27&sortBy=createdAt&sortDir=desc');

        $this->assertResponseStatusCodeSame(200);
        $this->assertJson($client->getResponse()->getContent());

        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('pageCount', $result);
        $this->assertSame(1, $result['pageCount'], 'Invalid page count.');
        $this->assertArrayHasKey('totalTaskCount', $result);
        $this->assertSame(2, $result['totalTaskCount'], 'Invalid total task count.');
        $this->assertArrayHasKey('tasks', $result);
        $this->assertIsArray($result['tasks']);
        $this->assertCount(2, $result['tasks'], 'Invalid number of tasks on page.');

        foreach ($result['tasks'] as $task) {
            TaskAssertion::assertTaskStructure($task);
        }
    }

    public function testReturnEmptyResultWhenPageIsOutOfRange(): void
    {
        $client = static::createClient();

        $this->getService(TaskStory::class)->givenManyTasksExist(2);

        $client->request('GET', '/api/v1/tasks?page=100');

        $this->assertResponseStatusCodeSame(200);
        $this->assertJson($client->getResponse()->getContent());

        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('pageCount', $result);
        $this->assertSame(1, $result['pageCount'], 'Invalid page count.');
        $this->assertArrayHasKey('totalTaskCount', $result);
        $this->assertSame(2, $result['totalTaskCount'], 'Invalid total task count.');
        $this->assertArrayHasKey('tasks', $result);
        $this->assertSame([], $result['tasks']);
    }

    /**
     * @depends testTasksWithoutFilteringFetchSuccessfully
     */
    public function testReturnDifferentResultsForDifferentPages(): void
    {
        $client = static::createClient();

        $this->getService(TaskStory::class)->givenManyTasksExist(4);

        $client->request('GET', '/api/v1/tasks?page=1');
        $this->assertResponseStatusCodeSame(200);
        $this->assertJson($client->getResponse()->getContent());
        $result = json_decode($client->getResponse()->getContent(), true);
        $tasksOnPage1 = $result['tasks'];

        $client->request('GET', '/api/v1/tasks?page=2');
        $this->assertResponseStatusCodeSame(200);
        $this->assertJson($client->getResponse()->getContent());
        $result = json_decode($client->getResponse()->getContent(), true);
        $tasksOnPage2 = $result['tasks'];

        $this->assertNotEquals($tasksOnPage1, $tasksOnPage2);
    }

    public function testReturnErrorResponseWhenInvalidFiltersAreSent(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/tasks?executionDay=&page=-1');

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(400)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 400,
                    'message' => 'The given data was invalid.',
                    'details' => [
                        'executionDay' => ['This value should not be blank.'],
                        'page' => ['This value should be of type digit|int.'],
                    ],
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }
}
