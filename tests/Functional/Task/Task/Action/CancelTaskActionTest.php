<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task\Action;

use App\System\ValueObject\Uuid\Uuid;
use Tests\Functional\Shared\Action\ActionTestCase;
use Tests\Functional\Shared\Action\ErrorResponseAssertion;
use Tests\Functional\Task\Task\TaskStory;

final class CancelTaskActionTest extends ActionTestCase
{
    public function testTaskCancelsSuccessfully(): void
    {
        $client = static::createClient();

        $taskId = Uuid::fromString('fcf9cb2c-5513-4ed4-8c9d-102f405bcfe6');

        $this->getService(TaskStory::class)->givenOpenTaskWithIdExists($taskId);

        $client->request('PATCH', '/api/v1/tasks/' . $taskId->toString() . '/cancel');

        $this->assertResponseStatusCodeSame(204);
        $this->assertEmpty($client->getResponse()->getContent());
    }

    public function testCannotCancelClosedTask(): void
    {
        $client = static::createClient();

        $taskId = Uuid::fromString('fcf9cb2c-5513-4ed4-8c9d-102f405bcfe6');

        $this->getService(TaskStory::class)->givenClosedTaskWithIdExists($taskId);

        $client->request('PATCH', '/api/v1/tasks/' . $taskId->toString() . '/cancel');

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(422)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 422,
                    'message' => 'Unable to cancel closed task',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }

    public function testNotFoundErrorResponseShouldBeReturnedWhenRequestedTaskDoesNotExist(): void
    {
        $nonexistentTaskId = '6fe37f5a-66e2-48ff-b57d-568f9262c755';
        $client = static::createClient();
        $client->request('PATCH', '/api/v1/tasks/' . $nonexistentTaskId . '/cancel');

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(404)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 404,
                    'message' => 'Task with ID "6fe37f5a-66e2-48ff-b57d-568f9262c755" does not exist',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }
}
