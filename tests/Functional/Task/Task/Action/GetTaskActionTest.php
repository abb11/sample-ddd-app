<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task\Action;

use App\System\ValueObject\Date\Date;
use App\System\ValueObject\Uuid\Uuid;
use Tests\Functional\Shared\Action\ActionTestCase;
use Tests\Functional\Shared\Action\ErrorResponseAssertion;
use Tests\Functional\Task\Task\TaskStory;

final class GetTaskActionTest extends ActionTestCase
{
    public function testTaskFetchesSuccessfully(): void
    {
        $client = static::createClient();

        $taskId = Uuid::fromString('21122006-46aa-411a-bebc-367436e8fede');

        $this->getService(TaskStory::class)->givenTaskWithDataExists([
            'id' => $taskId,
            'title' => 'New task',
            'priority' => 'low',
            'executionDay' => Date::fromString('2021-03-27'),
        ]);

        $client->request('GET', '/api/v1/tasks/' . $taskId->toString());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJson($client->getResponse()->getContent());

        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('task', $result);

        $task = $result['task'];
        TaskAssertion::assertTaskStructure($task);
        $this->assertSame('21122006-46aa-411a-bebc-367436e8fede', $task['id']);
        $this->assertSame('New task', $task['title']);
        $this->assertSame('open', $task['status']);
        $this->assertSame('low', $task['priority']);
        $this->assertSame('2021-03-27', $task['executionDay']);
    }

    public function testNotFoundErrorResponseShouldBeReturnedWhenInvalidUuidIsGiven(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/tasks/invalid-uuid');

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(404)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 404,
                    'message' => 'The UUID for the "taskId" parameter is invalid.',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }

    public function testNotFoundErrorResponseShouldBeReturnedWhenRequestedTaskDoesNotExist(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/tasks/03876d95-8d0d-4393-a689-1663e1a6a9f0');

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(404)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 404,
                    'message' => 'Task with ID "03876d95-8d0d-4393-a689-1663e1a6a9f0" does not exist',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }
}
