<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task\Action;

use PHPUnit\Framework\TestCase;
use Tests\Utils\Assertion\DateAssertion;

final class TaskAssertion
{
    public static function assertTaskStructure(mixed $task): void
    {
        TestCase::assertIsArray($task);
        TestCase::assertCount(6, $task);

        TestCase::assertArrayHasKey('id', $task);
        TestCase::assertIsString($task['id']);
        TestCase::assertNotEmpty($task['id']);

        TestCase::assertArrayHasKey('title', $task);
        TestCase::assertIsString($task['title']);
        TestCase::assertNotEmpty($task['title']);

        TestCase::assertArrayHasKey('priority', $task);
        TestCase::assertIsString($task['priority']);

        TestCase::assertArrayHasKey('status', $task);
        TestCase::assertIsString($task['status']);

        TestCase::assertArrayHasKey('createdAt', $task);
        DateAssertion::assertDateFormat($task['createdAt']);

        TestCase::assertArrayHasKey('executionDay', $task);

        if (null !== $task['executionDay']) {
            DateAssertion::assertDateFormat($task['executionDay'], 'Y-m-d');
        }
    }
}
