<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task\Action;

use App\System\ValueObject\Uuid\Uuid;
use App\Task\Application\Task\Projection\Dto\TaskDto;
use Tests\Functional\Shared\Action\ActionTestCase;
use Tests\Functional\Shared\Action\ErrorResponseAssertion;
use Tests\Functional\Task\Task\TaskStory;
use Tests\Functional\Task\Task\TaskTestHelper;

final class UpdateTaskActionTest extends ActionTestCase
{
    /**
     * @dataProvider provideNewTaskData
     */
    public function testTaskUpdatesSuccessfully(array $newTaskData): void
    {
        $client = static::createClient();
        $taskId = Uuid::fromString('21122006-46aa-411a-bebc-367436e8fede');

        $this->getService(TaskStory::class)->givenOpenTaskWithIdExists($taskId);

        $client->request('PUT', '/api/v1/tasks/' . $taskId->toString(), [], [], [], json_encode($newTaskData));

        $this->assertResponseStatusCodeSame(204);
        $this->assertEmpty($client->getResponse()->getContent());

        $updatedTask = $this->getService(TaskTestHelper::class)->getTask($taskId);
        $updatedTaskArray = $this->toArray($updatedTask);

        $this->assertEquals($newTaskData, $updatedTaskArray);
    }

    public function provideNewTaskData(): array
    {
        $params = [
            [
                'title' => 'Updated title 1',
                'priority' => 'normal',
                'executionDay' => null,
            ],
            [
                'title' => 'Updated title 2',
                'priority' => 'high',
                'executionDay' => date('Y-m-d'),
            ],
        ];

        return array_map(fn ($param) => [$param], $params);
    }

    private function toArray(TaskDto $task): array
    {
        return [
            'title' => $task->title,
            'priority' => $task->priority,
            'executionDay' => $task->executionDay,
        ];
    }

    public function testNotFoundErrorResponseShouldBeReturnedWhenInvalidUuidIsGiven(): void
    {
        $client = static::createClient();
        $client->request('PUT', '/api/v1/tasks/invalid-uuid', [], [], [], json_encode([
            'title' => 'Updated title',
            'priority' => 2,
            'executionDay' => null,
        ]));

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(404)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 404,
                    'message' => 'The UUID for the "taskId" parameter is invalid.',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }

    public function testNotFoundErrorResponseShouldBeReturnedWhenRequestedTaskDoesNotExist(): void
    {
        $nonexistentTaskId = '6fe37f5a-66e2-48ff-b57d-568f9262c755';
        $client = static::createClient();
        $client->request('PUT', '/api/v1/tasks/' . $nonexistentTaskId, [], [], [], json_encode([
            'title' => 'Updated title',
            'priority' => 'low',
            'executionDay' => null,
        ]));

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(404)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 404,
                    'message' => 'Task with ID "6fe37f5a-66e2-48ff-b57d-568f9262c755" does not exist',
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }
}
