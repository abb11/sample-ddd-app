<?php

declare(strict_types=1);

namespace Tests\Functional\Task\Task\Action;

use App\Task\Application\Task\Projection\Dto\TaskDto;
use Tests\Functional\Shared\Action\ActionTestCase;
use Tests\Functional\Shared\Action\ErrorResponseAssertion;
use Tests\Functional\Task\Task\TaskTestHelper;

final class CreateTaskActionTest extends ActionTestCase
{
    public function testTaskCreatesSuccessfully(): void
    {
        $client = static::createClient();
        $today = date('Y-m-d');
        $client->request('POST', '/api/v1/tasks', [], [], [], json_encode([
            'title' => 'Test',
            'priority' => 'normal',
            'executionDay' => $today,
        ]));

        $this->assertResponseStatusCodeSame(201);
        $this->assertJson($client->getResponse()->getContent());
        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('id', $result);
        $this->assertIsString($result['id']);

        /** @var TaskDto $createdTask */
        $createdTask = $this->getService(TaskTestHelper::class)->getTask($result['id']);
        $this->assertSame('Test', $createdTask->title);
        $this->assertSame('normal', $createdTask->priority);
        $this->assertSame($today, $createdTask->executionDay->toString());
    }

    public function testCannotCreateTaskWithInvalidData(): void
    {
        $client = static::createClient();
        $client->request('POST', '/api/v1/tasks', [], [], [], json_encode([
            'priority' => 1,
            'status' => 'NEW',
            'executionDay' => date('Y-m-d'),
        ]));

        ErrorResponseAssertion::assertThat($client->getResponse(), self::$kernel->isDebug())
            ->isJson()
            ->statusCodeIs(400)
            ->hasPayloadEqualTo([
                'error' => [
                    'status' => 400,
                    'message' => 'The given data was invalid.',
                    'details' => [
                        'title' => ['This field is missing.'],
                        'priority' => ['This value should be of type string.'],
                        'status' => ['This field was not expected.'],
                    ],
                ],
            ])
            ->hasNotEmptyHeader('X-Request-Id');
    }
}
