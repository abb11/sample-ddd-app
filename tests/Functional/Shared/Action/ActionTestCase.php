<?php

declare(strict_types=1);

namespace Tests\Functional\Shared\Action;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class ActionTestCase extends WebTestCase
{
    /**
     * @template T
     *
     * @param class-string<T> $serviceId
     *
     * @return T
     */
    protected function getService(string $serviceId): object
    {
        return static::getContainer()->get($serviceId);
    }
}
