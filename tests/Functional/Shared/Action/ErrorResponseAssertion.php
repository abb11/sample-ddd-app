<?php

declare(strict_types=1);

namespace Tests\Functional\Shared\Action;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

final class ErrorResponseAssertion
{
    private function __construct(
        private readonly Response $response,
        private readonly bool $debug,
    ) {
    }

    public static function assertThat(Response $response, bool $debug = false): self
    {
        return new self($response, $debug);
    }

    public function isJson(): self
    {
        TestCase::assertJson($this->response->getContent());

        return $this;
    }

    public function statusCodeIs(int $expectedStatusCode): self
    {
        TestCase::assertSame($expectedStatusCode, $this->response->getStatusCode(), 'Invalid status code.');

        return $this;
    }

    public function hasPayloadEqualTo(array $expectedPayload): self
    {
        $content = json_decode($this->response->getContent(), true);

        if (false === $this->debug) {
            TestCase::assertArrayNotHasKey('debug', $content, 'Response should not contain a debug info.');
        }

        unset($content['debug']);

        TestCase::assertEquals($expectedPayload, $content, 'Invalid response payload.');

        return $this;
    }

    public function hasNotEmptyHeader(string $name): self
    {
        TestCase::assertTrue($this->response->headers->has($name), sprintf('Missing "%s" header.', $name));
        TestCase::assertNotEmpty($this->response->headers->get($name));

        return $this;
    }
}
