<?php

declare(strict_types=1);

namespace Tests\Functional\Shared\Action;

final class HealthCheckActionTest extends ActionTestCase
{
    public function testHealthCheck(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(200);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertSame('{"status":"OK"}', $client->getResponse()->getContent());
        $this->assertTrue($client->getResponse()->headers->has('X-Request-Id'), 'Missing "X-Request-Id" header.');
        $this->assertNotEmpty($client->getResponse()->headers->get('X-Request-Id'));
    }
}
