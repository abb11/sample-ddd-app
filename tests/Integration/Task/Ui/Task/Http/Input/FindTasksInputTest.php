<?php

declare(strict_types=1);

namespace Tests\Integration\Task\Ui\Task\Http\Input;

use App\Task\Ui\Task\Http\Input\FindTasksInput;
use Tests\Integration\Shared\Ui\Http\Input\InputTestCase;

final class FindTasksInputTest extends InputTestCase
{
    protected function getInputClass(): string
    {
        return FindTasksInput::class;
    }

    public function provideValidData(): array
    {
        return [
            [
                [],
            ],
            [
                [
                    'executionDay' => '2021-04-18',
                ],
            ],
            [
                [
                    'page' => '1',
                ],
            ],
            [
                [
                    'sortBy' => 'executionDay',
                ],
            ],
            [
                [
                    'sortDir' => 'asc',
                ],
            ],
            [
                [
                    'sortDir' => 'desc',
                ],
            ],
            [
                [
                    'executionDay' => '2021-04-18',
                    'page' => '2',
                    'sortBy' => 'executionDay',
                    'sortDir' => 'desc',
                ],
            ],
        ];
    }

    public function provideInvalidData(): array
    {
        return [
            [
                'data' => [
                    'page' => '',
                    'executionDay' => '',
                    'sortBy' => '',
                    'sortDir' => '',
                ],
                'expectedErrorMessages' => [
                    'page' => ['This value should not be blank.'],
                    'executionDay' => ['This value should not be blank.'],
                    'sortBy' => ['This value should not be blank.'],
                    'sortDir' => ['This value should not be blank.'],
                ],
            ],
            [
                'data' => [
                    'page' => 'first',
                    'executionDay' => '18-04-2021',
                    'sortBy' => 'status',
                    'sortDir' => 'ascending',
                ],
                'expectedErrorMessages' => [
                    'page' => ['This value should be of type digit|int.'],
                    'executionDay' => ['This value is not a valid date.'],
                    'sortBy' => ['Sorting by "status" column is not allowed.'],
                    'sortDir' => ['The sort direction you choose is invalid.'],
                ],
            ],
            [
                'data' => [
                    'page' => '0',
                ],
                'expectedErrorMessages' => [
                    'page' => ['This value should be positive.'],
                ],
            ],
        ];
    }
}
