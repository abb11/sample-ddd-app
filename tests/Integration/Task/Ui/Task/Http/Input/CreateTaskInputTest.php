<?php

declare(strict_types=1);

namespace Tests\Integration\Task\Ui\Task\Http\Input;

use App\Task\Ui\Task\Http\Input\CreateTaskInput;
use Tests\Integration\Shared\Ui\Http\Input\InputTestCase;

final class CreateTaskInputTest extends InputTestCase
{
    protected function getInputClass(): string
    {
        return CreateTaskInput::class;
    }

    public function provideValidData(): array
    {
        return [
            [
                [
                    'title' => 'Test',
                    'priority' => 'low',
                    'executionDay' => '2021-04-05',
                ],
            ],
            [
                [
                    'title' => 'Test',
                    'priority' => 'normal',
                    'executionDay' => null,
                ],
            ],
        ];
    }

    public function provideInvalidData(): array
    {
        return [
            [
                'data' => [
                    'title' => true,
                    'priority' => 1,
                    'executionDay' => '05-04-2021',
                ],
                'expectedErrorMessages' => [
                    'title' => ['This value should be of type string.'],
                    'priority' => ['This value should be of type string.'],
                    'executionDay' => ['This value is not a valid date.'],
                ],
            ],
            [
                'data' => [
                    'title' => 'Test',
                    'priority' => 'low',
                    'executionDay' => '',
                ],
                'expectedErrorList' => [
                    'executionDay', // this field should not be blank
                ],
            ],
            [
                'data' => [
                    'title' => 'Test',
                    'priority' => 'high',
                    'status' => 'NEW',
                ],
                'expectedErrorList' => [
                    'executionDay', // missing field
                    'status', // unexpected field
                ],
            ],
        ];
    }
}
