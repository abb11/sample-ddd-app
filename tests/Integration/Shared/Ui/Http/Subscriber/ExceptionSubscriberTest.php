<?php

declare(strict_types=1);

namespace Tests\Integration\Shared\Ui\Http\Subscriber;

use App\Shared\Ui\Http\Subscriber\ExceptionSubscriber;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

final class ExceptionSubscriberTest extends KernelTestCase
{
    private ExceptionSubscriber $exceptionSubscriber;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->exceptionSubscriber = self::getContainer()->get(ExceptionSubscriber::class);
    }

    public function testItConvertExceptionsToJsonResponse(): void
    {
        $kernel = new TestKernel();
        $request = new Request();
        $exception = new \Exception('test');
        $event = new ExceptionEvent($kernel, $request, HttpKernelInterface::MAIN_REQUEST, $exception);

        $this->assertNull($event->getResponse());

        $this->exceptionSubscriber->handleKernelException($event);

        $this->assertInstanceOf(JsonResponse::class, $event->getResponse(), 'A JsonResponse should be set.');
    }
}

class TestKernel implements HttpKernelInterface
{
    public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = true): Response
    {
        return new Response('foo');
    }
}
