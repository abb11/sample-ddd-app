<?php

declare(strict_types=1);

namespace Tests\Integration\Shared\Ui\Http\Input;

use App\System\Exception\ValidationException;
use App\System\Request\AbstractInput;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Base class for test data inputs
 */
abstract class InputTestCase extends KernelTestCase
{
    protected ValidatorInterface $validator;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->validator = static::getContainer()->get(ValidatorInterface::class);
    }

    /**
     * Test input with valid data
     *
     * @param array $data Input data
     *
     * @dataProvider provideValidData
     */
    public function testCreateInputWithValidData(array $data): void
    {
        /** @var class-string<AbstractInput> $inputClass */
        $inputClass = $this->getInputClass();
        try {
            $input = $inputClass::create($this->validator, $data);
            $this->assertInstanceOf($this->getInputClass(), $input);
        } catch (ValidationException $exception) {
            $this->fail($exception->getMessage() . "\n" . $exception->getViolations());
        }
    }

    /**
     * Test input with invalid data
     *
     * @param array $data           Input data
     * @param array $expectedErrors Expected errors
     *
     * @dataProvider provideInvalidData
     */
    public function testCreateInputWithInvalidData(array $data, array $expectedErrors): void
    {
        $this->expectException(ValidationException::class);
        /** @var class-string<AbstractInput> $inputClass */
        $inputClass = $this->getInputClass();
        try {
            $inputClass::create($this->validator, $data);
        } catch (ValidationException $exception) {
            $this->assertViolations($expectedErrors, $exception->getViolations());
            throw $exception;
        }
    }

    protected function assertViolations(array $expectedErrors, ConstraintViolationListInterface $violationList): void
    {
        $violations = [];

        /** @var ConstraintViolationInterface $violation */
        foreach ($violationList as $violation) {
            $propPath = trim(str_replace('][', '.', $violation->getPropertyPath()), '[]');
            $violations[$propPath][] = $violation->getMessage();
        }

        if (isset($expectedErrors[0])) {
            $this->assertEquals($expectedErrors, array_keys($violations), 'Wrong error list.');
        } else {
            $this->assertEquals($expectedErrors, $violations, 'Wrong error messages.');
        }
    }

    abstract protected function getInputClass(): string;

    abstract public function provideValidData(): array;

    abstract public function provideInvalidData(): array;
}
