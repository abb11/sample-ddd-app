# Sample DDD application

Example of a RESTful application using Domain-Driven Design (DDD) and Command Query Responsibility Segregation (CQRS)
principles and hexagonal architecture pattern to keep the code as simple as possible.

## Stack

* Docker
* PHP 8.1
* Nginx
* Postgres 13
* Swagger UI
* Symfony 6
* Doctrine ORM

Command bus, query bus, and event bus base on Symfony Messenger component.
OpenAPI 3 standard is used for API documentation.
Makefile recipes help you manage the project (run `make help` to list all available recipes).

## How to install this project

1. Create config file (`cp .env.dist .env`) and customize it, in particular set `HOST_IP` if you want to use xdebug
in dev mode and `DOCKER_USER_ID` to resolve problems with file permissions
2. Run `make build` to build Docker images
3. Run `make up` to start Docker containers (detached mode without logs)
4. Run `make setup-app` to install dependencies, execute database migrations, etc.
5. Run `make swagger-build` to generate OpenAPI documentation
6. Now, two addresses should be available (by default): API <http://localhost:8800>, documentation <http://localhost:8801>
7. Run `make down` to stop the Docker containers

## Run domain event publisher worker

1. Run `make run-event-publisher-worker` to send all unpublished events to the message broker

## Tests

1. Run `make init-test-db` to initialize the database for tests 
1. Run `make tests` to execute tests

## Static code analysis

1. Run `make php-cs` to detect violations of a coding standard
1. Run `make php-cs-fix` to fix PHP code to follow a coding standard
1. Run `make phpstan` to perform source code analysis

## Debugging

Set the `HOST_IP` environment variable to your host machine IP and configure
[path mapping](https://www.jetbrains.com/help/phpstorm/troubleshooting-php-debugging.html#no-mappings)
in your PhpStorm.
To tell PhpStorm which path mapping configuration should be used for debugging, the server name
(in PHP | Servers dialog) should be equal to Docker's `COMPOSE_PROJECT_NAME` environment variable value 
(default sample-ddd-app).
