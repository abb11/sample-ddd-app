<?php

declare(strict_types=1);

$finder = (new PhpCsFixer\Finder())
    ->in('src')
    ->in('tests')
    ->in('migrations')
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'class_definition' => [
            'inline_constructor_arguments' => false,
            'multi_line_extends_each_single_line' => true,
        ],
        'concat_space' => [
            'spacing' => 'one',
        ],
        'operator_linebreak' => [
            'position' => 'beginning',
        ],
        'ordered_imports' => [
            'imports_order' =>  [
                'const',
                'class',
                'function',
            ],
        ],
        'align_multiline_comment' => true,
        'array_indentation' => true,
        'declare_strict_types' => true,
        'method_chaining_indentation' => true,
        'no_unreachable_default_argument_value' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'phpdoc_var_annotation_correct_order' => true,
        'return_assignment' => true,
        'phpdoc_summary' => false,
        'protected_to_private' => false,
        'single_line_throw' => false,
    ])
    ->setRiskyAllowed(true)
    ->setFinder($finder)
    ->setCacheFile(__DIR__ . '/.php-cs-fixer.cache')
;
