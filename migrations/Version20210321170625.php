<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210321170625 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creates "public.task" table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE public.task (
                id uuid NOT NULL,
                title text NOT NULL,
                status varchar(50) NOT NULL,
                priority varchar(50) NOT NULL,
                created_at timestamp NOT NULL DEFAULT now(),
                execution_day date,
                CONSTRAINT task_id_pkey PRIMARY KEY (id)
            )
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE public.task');
    }
}
