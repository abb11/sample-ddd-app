<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210327163413 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creates table to store events';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE public.event_store (
                id uuid NOT NULL,
                publisher_type text NOT NULL,
                publisher_id uuid NOT NULL,
                event_type text NOT NULL,
                event_payload jsonb NOT NULL,
                created_at timestamp NOT NULL DEFAULT now(),
                published_at timestamp,
                CONSTRAINT event_store_id_pkey PRIMARY KEY (id)
            )
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE public.event_store');
    }
}
