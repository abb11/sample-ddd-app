<?php

namespace PHPSTORM_META {
    override(\App\Shared\Ui\Http\Action\AbstractHttpAction::executeQuery(0), map([
        '' => '@Result',
    ]));
    override(\Symfony\Component\Serializer\Normalizer\DenormalizerInterface::denormalize(), type(1));
    override(\Psr\Container\ContainerInterface::get(), type(0));
    override(\App\System\Request\InputFactoryInterface::createFromRequest(), type(1));
    override(\App\System\Request\InputFactoryInterface::createFromArray(), type(1));
}
