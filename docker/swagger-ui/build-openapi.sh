#!/bin/sh

set -e
swagger-cli bundle /swagger/index.yaml -o /usr/share/nginx/html/openapi.yaml --type yaml
