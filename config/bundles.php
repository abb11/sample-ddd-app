<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    DAMA\DoctrineTestBundle\DAMADoctrineTestBundle::class => ['test' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    Chrisguitarguy\RequestId\ChrisguitarguyRequestIdBundle::class => ['all' => true],
    Grifix\MemoryBundle\GrifixMemoryBundle::class => ['all' => true],
    Grifix\ClockBundle\GrifixClockBundle::class => ['all' => true],
    Grifix\WorkerBundle\GrifixWorkerBundle::class => ['all' => true],
];
